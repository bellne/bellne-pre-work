﻿using System;
using System.Data.SqlClient;
using System.Configuration;

namespace DataReaderExample
{
    class Program
    {
        static void Main()
        {
            DisplayCompanyNameUSA();
            string userInput = GetUserInput();
            Console.ReadLine();
            UpdateContactName(userInput);


            //DisplayCompanyNameUSAWithParameter();
            //Console.ReadLine();
            //DisplayEmployeeList();
            //Console.ReadLine();
            //DisplayEmployeesAndTheirManagers();
            //Console.ReadLine();
        }

        private static string GetUserInput()
        {
            Console.WriteLine("\nEnter the name of column to update: ");
            string userInput = Console.ReadLine();
            return userInput;
        }

        private static void UpdateContactName(string userInput)
        {

        }

        private static void DisplayCompanyNameUSA()
        {
            string connectionString;
            connectionString = ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "SELECT CompanyName, ContactName, OrderDate, ProductName, Country " +
                                        "FROM Orders INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID " +
                                        "INNER JOIN[Order Details] ON Orders.OrderID = [Order Details].OrderID " +
                                        "INNER JOIN Products ON[Order Details].ProductID = Products.ProductID " +
                                        "WHERE Customers.Country = 'USA' and CompanyName = 'Lonesome Pine Restaurant'";
                sqlCommand.Connection = sqlConnection;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    Console.WriteLine("{0}, {1}, {2}, {3}, {4}", "CompanyName","ContactName","OrderDate","ProductName","Country");
                    while (sqlDataReader.Read())
                    {
                        Console.WriteLine("{0}, {1}, {2}, {3}, {4}", sqlDataReader["CompanyName"].ToString(),
                                                                    sqlDataReader["ContactName"].ToString(),
                                                                    sqlDataReader["OrderDate"].ToString(),
                                                                    sqlDataReader["ProductName"].ToString(),
                                                                    sqlDataReader["Country"].ToString());
                    }
                }
            }
        }

        private static void DisplayCompanyNameUSAWithParameter()
        {
            string connectionString;
            connectionString = ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand();
                string @commandText = "SELECT CompanyName, ContactName, OrderDate, ProductName, Country " +
                                        "FROM Orders INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID " +
                                        "INNER JOIN[Order Details] ON Orders.OrderID = [Order Details].OrderID " +
                                        "INNER JOIN Products ON[Order Details].ProductID = Products.ProductID " +
                                        "WHERE Customers.Country = 'USA' and CompanyName = 'Lonesome Pine Restaurant'";
                sqlCommand.CommandText = commandText;
                sqlCommand.Connection = sqlConnection;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        Console.WriteLine("{0}, {1}, {2}, {3}, {4}", sqlDataReader["CompanyName"].ToString(),
                                                                    sqlDataReader["ContactName"].ToString(),
                                                                    sqlDataReader["OrderDate"].ToString(),
                                                                    sqlDataReader["ProductName"].ToString(),
                                                                    sqlDataReader["Country"].ToString());
                    }
                }
            }
        }



        private static void DisplayEmployeeList()
        {
        
            ConnectionStringSettings settings = ConfigurationManager
                                    .ConnectionStrings["Northwind"];

            var connectionString = settings.ConnectionString;

            // create connection with connection string
            // using to automatically close it
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    // Create a command
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandText = "SELECT * FROM Employees";
                    sqlCommand.Connection = sqlConnection;

                    sqlConnection.Open(); // must have open connection to query

                    // call ExecuteReader to have the command create a 
                    // data reader, this executes the SQL Statement
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        //Read() returns false when there is no more data
                        while (sqlDataReader.Read())
                        {
                            Console.WriteLine("{0} {1}, {2}",
                                sqlDataReader["EmployeeID"].ToString(),
                                sqlDataReader["LastName"].ToString(),
                                sqlDataReader["FirstName"].ToString());                            
                        }
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Enter to continue...");
                Console.ReadLine();
            }

        }

        private static void DisplayEmployeesAndTheirManagers()
        {
            string connectionString;
            connectionString = ConfigurationManager
                                .ConnectionStrings["Northwind"]
                                .ConnectionString;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "select e1.LastName, e1.FirstName, e2.firstname as 'Manager' from Employees e1 inner join Employees e2 on e1.reportsto = e2.EmployeeID where EmployeeID = @id";

                var userInput = "some malicious sql";
                sqlCommand.Parameters.Add(new SqlParameter("@id", userInput));
                sqlCommand.Connection = sqlConnection;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {

                        Console.WriteLine("{0}, {1}, {2}",
                            sqlDataReader["firstname"].ToString(),
                            sqlDataReader["lastname"].ToString(),
                            sqlDataReader["Manager"].ToString());
                    }
                }
            }
        }
    }
}
