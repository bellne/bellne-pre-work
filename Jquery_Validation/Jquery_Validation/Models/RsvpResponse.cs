﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jquery_Validation.Models
{
    public class RsvpResponse
    {
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@"^\S+@\S+$", ErrorMessage = "The email address format isn't valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        [RegularExpression(@"\d{3}-\d{3}-\d{4}", ErrorMessage = "The phone number format isn't valid")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter your favorite game")]
        public string FavoriteGame { get; set; }

        [Required(ErrorMessage = "Please specify whether you will attend")]
        public bool? WillAttend { get; set; }
    }
}