Product Inventory Project
Difficulty: 3

Create a program that manages multiple products such as shirts, pants, coats and other products. Allow the user to add products to inventory, add/remove stock to an existing product, calculate the total value of a product and the total value of the entire inventory. This program is essentially a product shop in a box. It could be a department store, a specialty shop or a grocery store.


Tips
-------------------------------
This is good for practicing the idea of Object Oriented Programming (OOP), inheritance and polymorphism. It starts off with the creation of a �Product� base class and from there you inherit various types of products. Each of these products are then stored in an �Inventory� class. Start first by creating the Product base class. What would be common to all products? A title, description, price perhaps. Make sure you fully flesh out this base class and keep in mind that you will inherit from it. A product called �pants� is a type of product. A �shirt� is also a type of
product. So they must both share the idea of what it means to be a product. Once you have this done, go to fleshing out the Inventory class which will have a list (array, arraylist, list etc) of Product classes. Since a shirt is a product, it can be stored in this array long with any other type of class that inherits from �Product�. You want to be able to do products[0] = new Shirt(); where the products list is of type �Product�.

Added Difficulty
-------------------------------
When a product falls below a certain number, have the system tell the user that they need to restock.