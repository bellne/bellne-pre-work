Create DATABASE	MovieCatalog
GO

USE MovieCatalog
GO

CREATE TABLE Movie
(
	MovieId int identity(1,1) not null primary key,
	Title varchar(50) not null,
	RunTime int null,
	Rating varchar(5) null,
	ReleaseDate date null
)