﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Date_Time_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime firstDateParsed;
            DateTime secondDateParsed;

            Console.WriteLine("Enter the later of the two dates in mm/dd/yyyy format: ");
            string firstDate = Console.ReadLine();
            DateTime.TryParse(firstDate, out firstDateParsed);

            Console.WriteLine("Now enter the earlier of the two date in mm/dd/yyyy format: ");
            string secondDate = Console.ReadLine();
            DateTime.TryParse(secondDate, out secondDateParsed);

            TimeSpan numDaysBetweenDates = firstDateParsed.Subtract(secondDateParsed);
            int numWednesdays = numDaysBetweenDates.Days / 7;

            Console.WriteLine("There are {0:N} Wednesdays between {1} and {2}", numWednesdays, 
                firstDateParsed.ToString("d"), secondDateParsed.ToString("d"));
            Console.ReadLine();
        }
    }
}
