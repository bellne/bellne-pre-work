﻿using System.Collections.Generic;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.Data.Mocks
{
    public class MockConversionTypeRepository : IConversionTypeRepository
    {
        public List<ConversionType> GetAll()
        {
            return new List<ConversionType>()
            {
                new ConversionType() {ConversionTypeId = 1, ConversionTypeName = "Temperature"},
                new ConversionType() {ConversionTypeId = 2, ConversionTypeName = "Currency"}
            };
        }
    }
}
