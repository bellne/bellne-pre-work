﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.Data.Mocks
{
    public class MockMovieRepository : IMovieRepository
    {
        private static List<Movie> _allMovies;

        static MockMovieRepository()
        {
            _allMovies = new List<Movie>()
            {
                new Movie() { MovieID = 1, Title = "Ghostbusters", Rating = "PG-13", RunTime = 89},
                new Movie() { MovieID=2, Title = "Groundhog Day", Rating="PG", RunTime = 94, ReleaseDate = new DateTime(1980, 6,5)}
            };
        }

        public List<Movie> GetAll()
        {
            return _allMovies;
        }

        public Movie GetById(int id)
        {
            return _allMovies.FirstOrDefault(m => m.MovieID == id);
        }

        public void Add(Movie movie)
        {
            movie.MovieID = _allMovies.Max(m => m.MovieID) + 1;
            _allMovies.Add(movie);
        }

        public void Edit(Movie movie)
        {
            _allMovies.RemoveAll(m => m.MovieID == movie.MovieID);
            _allMovies.Add(movie);
        }

        public void Delete(int id)
        {
            _allMovies.RemoveAll(m => m.MovieID == id);
        }
    }
}
