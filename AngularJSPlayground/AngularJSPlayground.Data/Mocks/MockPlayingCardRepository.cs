﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.Data.Mocks
{
    public class MockPlayingCardRepository : IPlayingCardRepository
    {
        public List<PlayingCard> GetCards()
        {
            return new List<PlayingCard>()
            {
                new PlayingCard() { CardId = 1, Image = "/Img/PlayingCards/1.png", Suite = "Clubs", Value = "Ace", Score=11},
                new PlayingCard() { CardId = 2, Image = "/Img/PlayingCards/2.png", Suite = "Spades", Value = "Ace", Score=11},
                new PlayingCard() { CardId = 3, Image = "/Img/PlayingCards/3.png", Suite = "Hearts", Value = "Ace", Score=11},
                new PlayingCard() { CardId = 4, Image = "/Img/PlayingCards/4.png", Suite = "Diamonds", Value = "Ace", Score=11},
                new PlayingCard() { CardId = 5, Image = "/Img/PlayingCards/5.png", Suite = "Clubs", Value = "King", Score=10},
                new PlayingCard() { CardId = 6, Image = "/Img/PlayingCards/10.png", Suite = "Spades", Value = "Queen", Score=10},
                new PlayingCard() { CardId = 7, Image = "/Img/PlayingCards/24.png", Suite = "Diamonds", Value = "9", Score=9},
                new PlayingCard() { CardId = 8, Image = "/Img/PlayingCards/29.png", Suite = "Clubs", Value = "7", Score=7},
            };
        }
    }
}
