﻿using System.Collections.Generic;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.Data.Mocks
{
    public class MockUnitRepository : IUnitRepository
    {

        public List<Unit> GetForConversionType(int id)
        {
            if (id == 1)
            {
                return new List<Unit>
                {
                    new Unit() {UnitId = 1, UnitName = "Celcuis to Fahrenheit"},
                    new Unit() {UnitId = 2, UnitName = "Fahrenheit to Celcius"},
                    new Unit() {UnitId = 3, UnitName = "Kelvin to Fahrenheit"},
                    new Unit() {UnitId = 4, UnitName = "Fahrenheit to Kelvin"}
                };
            }
            else if (id == 2)
            {
                return new List<Unit>
                {
                    new Unit() {UnitId = 5, UnitName = "USD to CAD"},
                    new Unit() {UnitId = 6, UnitName = "USD to EUR"},
                    new Unit() {UnitId = 7, UnitName = "USD to SEK"},
                };
            }

            // bad id, return empty list
            return new List<Unit>();
        }
    }
}
