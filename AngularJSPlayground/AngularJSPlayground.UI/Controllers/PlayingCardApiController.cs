﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.UI.Controllers
{
    public class PlayingCardApiController : ApiController
    {
        private readonly IPlayingCardRepository _playingCardRepository;

        public PlayingCardApiController(IPlayingCardRepository playingCardRepository)
        {
            _playingCardRepository = playingCardRepository;
        }

        public List<PlayingCard> Get()
        {
            return _playingCardRepository.GetCards();
        }
    }
}
