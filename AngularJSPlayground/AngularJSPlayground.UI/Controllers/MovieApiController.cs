﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.UI.Controllers
{
    public class MovieApiController : ApiController
    {
        private readonly IMovieRepository _movieRepository;

        public MovieApiController(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public List<Movie> Get()
        {
            return _movieRepository.GetAll();
        }

        public Movie Get(int id)
        {
            return _movieRepository.GetById(id);
        }

        public HttpResponseMessage Post(Movie movie)
        {
            _movieRepository.Add(movie);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public HttpResponseMessage Put(Movie movie)
        {
            _movieRepository.Edit(movie);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        public HttpResponseMessage Delete(int id)
        {
            _movieRepository.Delete(id);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
