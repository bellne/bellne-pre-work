﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSPlayground.Models;
using AngularJSPlayground.Models.Interfaces;

namespace AngularJSPlayground.UI.Controllers
{
    public class ConversionApiController : ApiController
    {
        private readonly IConversionTypeRepository _conversionTypeRepository;
        private readonly IUnitRepository _unitRepository;

        public ConversionApiController(IConversionTypeRepository conversionTypeRepository, IUnitRepository unitRepository)
        {
            _conversionTypeRepository = conversionTypeRepository;
            _unitRepository = unitRepository;
        }

        public List<ConversionType> Get()
        {
            return _conversionTypeRepository.GetAll();
        }

        public List<Unit> Get(int id)
        {
            return _unitRepository.GetForConversionType(id);
        }
    }
}
