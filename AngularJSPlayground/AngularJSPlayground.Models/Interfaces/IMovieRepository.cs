﻿using System.Collections.Generic;

namespace AngularJSPlayground.Models.Interfaces
{
    public interface IMovieRepository
    {
        List<Movie> GetAll();
        Movie GetById(int id);
        void Add(Movie movie);
        void Edit(Movie movie);
        void Delete(int id);
    }
}
