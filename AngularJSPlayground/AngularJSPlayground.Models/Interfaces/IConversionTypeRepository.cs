﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSPlayground.Models.Interfaces
{
    public interface IConversionTypeRepository
    {
        List<ConversionType> GetAll();
    }
}
