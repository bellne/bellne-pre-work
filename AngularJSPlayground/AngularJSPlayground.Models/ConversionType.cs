﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSPlayground.Models
{
    public class ConversionType
    {
        public int ConversionTypeId { get; set; }
        public string ConversionTypeName { get; set; }
    }
}
