﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSPlayground.Models
{
    public class PlayingCard
    {
        public int CardId { get; set; }
        public string Suite { get; set; }
        public string Value { get; set; }
        public string Image { get; set; }
        public int Score { get; set; }
    }
}
