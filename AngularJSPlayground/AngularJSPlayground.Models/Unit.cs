﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSPlayground.Models
{
    public class Unit
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
    }
}
