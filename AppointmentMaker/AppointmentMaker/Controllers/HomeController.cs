﻿using AppointmentMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppointmentMaker.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult MakeBooking()
        {
            var model = new Appointment() { Date = DateTime.Today };

            return View(model);
        }

        [HttpPost]
        public ActionResult MakeBooking(Appointment model)
        {
            //if (string.IsNullOrEmpty(model.ClientName))
            //{
            //    ModelState.AddModelError("ClientName", "Please enter your name");
            //}

            //if(ModelState.IsValidField("Date") && DateTime.Now > model.Date)
            //{
            //    ModelState.AddModelError("Date", "Please enter a date in the future");
            //}

            //if (!model.TermsAccepted)
            //{
            //    ModelState.AddModelError("TermsAccepted", "You must accept the terms");
            //}

            //if(ModelState.IsValidField("ClientName") && ModelState.IsValidField("Date"))
            //{
            //    if (model.ClientName == "Garfield" && model.Date.DayOfWeek == DayOfWeek.Monday)
            //    {
            //        ModelState.AddModelError("", "Garfield cannot book appointments on Mondays");
            //    }
            //}

            if (ModelState.IsValid) //this tells the program to run the message checks in the css
            {
                return View("Completed", model);
            }

            return View();
        }
    }
}