﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AppointmentMaker.Attributes;

namespace AppointmentMaker.Models
{
    //Ivaldiateobject is the most versatile error checking mechanism
    //[NoGarfieldOnMondays]
    public class Appointment : IValidatableObject
    {
        //[Required]
        public string ClientName { get; set; }

        [DataType(DataType.Date)]
        //[FutureDate(ErrorMessage = "Please enter a date in the future")]
        public DateTime Date { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must accept the terms")] *the code below this makes it much simpler
        //[MustBeTrue( ErrorMessage = "You must accept the terms")]
        public bool TermsAccepted { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(ClientName))
            {
                errors.Add(new ValidationResult("Please enter your name", new[] { "ClientName" }));
            }

            if(DateTime.Now > Date)
            {
                errors.Add(new ValidationResult("Please enter a date in the future", new[] { "Date" }));
            }

            if(errors.Count == 0 && ClientName == "Garfield" && Date.DayOfWeek == DayOfWeek.Monday)
            {
                errors.Add(new ValidationResult("Garfield cannot book on Mondays"));
            }

            if (!TermsAccepted)
            {
                errors.Add(new ValidationResult("Please accept the terms", new[] { "TermsAccepted" }));
            }

            return errors;
        }
    }
}