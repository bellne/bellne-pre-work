﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AppointmentMaker.Attributes
{
    public class FutureDateAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            // base.isvalid makes sure that there is a date entered
            return base.IsValid(value) && ((DateTime)value) > DateTime.Today;
        }
    }
}