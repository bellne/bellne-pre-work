﻿using AppointmentMaker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentMaker.Attributes
{
    public class NoGarfieldOnMondaysAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            //as attempts to convert a value, and if it fails it returns null
            Appointment model = value as Appointment;

            if (model == null || string.IsNullOrEmpty(model.ClientName))
            {
                //we don't have a model of the right type, so just move on
                return true;
            }

            return !(model.ClientName == "Garfield" && model.Date.DayOfWeek == DayOfWeek.Monday);
        }
    }
}