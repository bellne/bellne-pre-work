﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhonebookRevamped.Models
{
    public class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string FriendRating { get; set; }
        public int StarPower { get; set; }
    }
}