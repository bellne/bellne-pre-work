﻿using PhonebookRevamped.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhonebookRevamped.Controllers
{
    public class ContactController : Controller
    {
        // Get  
        public ActionResult Index()
        {
            var model = new Contact();
            var model2 = new AddContactVM();
            var model3 = new ContactRepository();
            return View();
        }

        public ActionResult Add()
        {
            var model = new AddContactVM();
            model.Contact = new Contact();

            return View(model);
        }

        [HttpPost]
        public ActionResult PostContact(AddContactVM newContact)
        {
            ContactRepository.InsertContact(newContact.Contact);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult DeleteContact(int id)
        {
            ContactRepository.DeleteContact(id);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int id)
        {
            var model = ContactRepository.Get(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Contact contact)
        {
            ContactRepository.EditContact(contact);

            return RedirectToAction("Index", "Home");
        }
    }
}