﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAndModeling3.Products
{
    public class Product
    {
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
