﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor.cs
{
    class Program
    {
        static void Main(string[] args)
        {
            string userInput;
            int userNum;
            int numFactors = 0;
            int perfect = 0;
            List<int> allFactors = new List<int>();

            Console.WriteLine("Welcome to F-A-C-T-O-R-I-Z-O-R!\n\nPlease enter a positive number: ");
            userInput = Console.ReadLine();

            if (int.TryParse(userInput, out userNum))
            {
                for (int i = 1; i < userNum; i++)
                {
                    if (userNum % i == 0)
                    {
                        numFactors += 1;
                        perfect += i;
                        allFactors.Add(i);
                    }
                }                
            }
            else
            {
                Console.WriteLine("That's not a positive number!");
            }

            Console.WriteLine("\nRESULTS:");
            Console.WriteLine(" *Number entered: {0}", userNum);
            Console.WriteLine(" *Number of factors: {0}", numFactors);
            Console.WriteLine(" *List of factors:");
                foreach (int item in allFactors)
                {
                    Console.WriteLine("   Number: {0}", item);
                }
            if (perfect == userNum)
            {
                Console.WriteLine(" *This IS a perfect number!");
            }
            else
            {
                Console.WriteLine(" *This is NOT a perfect number.");
            }
           if (numFactors == 1)
            {
                Console.WriteLine(" *Prime: Yes");
            }
            else
            {
                Console.WriteLine(" *Prime: No");
            }

            Console.ReadLine();

        }
    }
}
