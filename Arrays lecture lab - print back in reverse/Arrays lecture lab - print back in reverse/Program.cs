﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays_lecture_lab___print_back_in_reverse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Write a message to see some programming magic.");
            string userInput = Console.ReadLine();
            char[] arr = userInput.ToCharArray();
            Array.Reverse(arr);
            Console.WriteLine(arr);
            Console.ReadLine();
        }
    }
}
