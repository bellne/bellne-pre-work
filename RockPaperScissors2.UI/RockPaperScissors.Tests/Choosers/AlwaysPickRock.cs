﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors2.UI;

namespace RockPaperScissors.Tests.Choosers
{
    public class AlwaysPickRock : IChooser
    {
        public Choice GetChoice()
        {
            return Choice.Rock;
        }
    }
}
