﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RockPaperScissors2.UI;
using RockPaperScissors.Tests.Choosers;

namespace RockPaperScissors.Tests
{
    [TestFixture]
    public class GameTests
    {
        [Test]
        public void RockBeatsScissors()
        {
            Game g = new Game(new AlwaysPickRock(), new AlwaysPickScissors());

            MatchResult r = g.Play();
            Assert.AreEqual(MatchResult.Player1Win, r);

            g = new Game(new AlwaysPickScissors(), new AlwaysPickRock());

            r = g.Play();
            Assert.AreEqual(MatchResult.Player2Win, r);
        }

        [Test]
        public void PaperBeatsRock()
        {
            Game g = new Game(new AlwaysPickPaper(), new AlwaysPickRock());

            MatchResult r = g.Play();
            Assert.AreEqual(MatchResult.Player1Win, r);

            g = new Game(new AlwaysPickRock(), new AlwaysPickPaper());

            r = g.Play();
            Assert.AreEqual(MatchResult.Player2Win, r);
        }

        [Test]
        public void ScissorsBeatsPaper()
        {
            Game g = new Game(new AlwaysPickScissors(), new AlwaysPickPaper());

            MatchResult r = g.Play();
            Assert.AreEqual(MatchResult.Player1Win, r);

            g = new Game(new AlwaysPickPaper(), new AlwaysPickScissors());

            r = g.Play();
            Assert.AreEqual(MatchResult.Player2Win, r);
        }

        [Test]
        public void Draw()
        {
            Game g = new Game(new AlwaysPickPaper(), new AlwaysPickPaper());

            MatchResult r = g.Play();
            Assert.AreEqual(MatchResult.Draw, r);

            g = new Game(new AlwaysPickRock(), new AlwaysPickRock());

            r = g.Play();
            Assert.AreEqual(MatchResult.Draw, r);
        }
    }
}
