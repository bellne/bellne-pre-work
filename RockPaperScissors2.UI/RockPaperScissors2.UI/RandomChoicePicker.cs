﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors2.UI
{
    public class RandomChoicePicker : IChooser
    {
        private static Random _rng = new Random();

        public Choice GetChoice()
        {
            switch (_rng.Next(1, 4))
            {
                case 1:
                    return Choice.Rock;
                case 2:
                    return Choice.Scissors;
                default:
                    return Choice.Paper;
            }
        }
    }
}
