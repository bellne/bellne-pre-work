﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace RockPaperScissors2.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfPlayers = int.Parse(ConfigurationManager.AppSettings["players"]);

            Game g = GameFactory.CreateGame(numberOfPlayers);

            MatchResult r = g.Play();

            switch (r)
            {
                case MatchResult.Draw:
                    Console.WriteLine("It was a draw!");
                    break;
                case MatchResult.Player1Win:
                    Console.WriteLine("Player 1 wins!");
                    break;
                default:
                    Console.WriteLine("Player 2 wins!");
                    break;
            }

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }
    }
}
