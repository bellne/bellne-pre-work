﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors2.UI
{
    public class HumanChoicePicker : IChooser
    {
        public Choice GetChoice()
        {
            Console.Write("Enter your choice (R, P, S): ");
            string input = Console.ReadLine();

            switch (input)
            {
                case "R":
                    return Choice.Rock;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Paper;
            }
        }        
    }
}
