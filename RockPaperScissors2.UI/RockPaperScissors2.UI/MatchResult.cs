﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors2.UI
{
    public enum MatchResult
    {
        Player1Win,
        Player2Win,
        Draw
    }
}
