﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors2.UI
{
    public class GameFactory
    {
        public static Game CreateGame(int numberOfPlayers)
        {
            switch (numberOfPlayers)
            {
                case 0:
                    return new Game(new RandomChoicePicker(), new RandomChoicePicker());
                case 1:
                    return new Game(new HumanChoicePicker(), new RandomChoicePicker());
                default:
                    return new Game(new HumanChoicePicker(), new HumanChoicePicker());
            }
        }
    }
}
