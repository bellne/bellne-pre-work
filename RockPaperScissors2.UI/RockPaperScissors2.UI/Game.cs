﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors2.UI
{
    public class Game
    {
        private IChooser _player1Chooser;
        private IChooser _player2Chooser;

        public Game(IChooser player1Chooser, IChooser player2Chooser)
        {
            _player1Chooser = player1Chooser;
            _player2Chooser = player2Chooser;
        }

        public MatchResult Play()
        {
            Choice p1Choice;
            Choice p2Choice;

            p1Choice = _player1Chooser.GetChoice();
            p2Choice = _player2Chooser.GetChoice();

            Console.WriteLine("Player 1 picked: {0}, Player 2 picked: {1}",
                Enum.GetName(typeof(Choice), p1Choice),
                Enum.GetName(typeof(Choice), p2Choice));

            if (p1Choice == p2Choice)
            {
                return MatchResult.Draw;
            }
            if (p1Choice == Choice.Rock && p2Choice == Choice.Scissors ||
               p1Choice == Choice.Scissors && p2Choice == Choice.Paper ||
               p1Choice == Choice.Paper && p2Choice == Choice.Rock)
                return MatchResult.Player1Win;
            else
            {
                return MatchResult.Player2Win;
            }
        }            
    }
}
