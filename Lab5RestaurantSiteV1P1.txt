<!DOCTYPE html>

<html>
	<head>
		<title>RestaurantSiteV1</title>
	</head>
	<body>
		<h4 id="home">Home</h4>
		<h1><center>Bellas</center></h1>
		<hr>

		<a href="#home"><b>Home</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#menu"><b>Menu</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#contactUs"><b>Contact Us</b></a>

		<hr><br>

		<center><img src="https://s-media-cache-ak0.pinimg.com/736x/1a/05/46/1a054614b2cb56dc6f865ddb0f9bb321.jpg" alt="Bellas Exterior" style="width: 400px; height: 150px;"></center>

		<center><p align="left"; style="border:1px; border-style:solid; border-color:black; padding: 1em; width:365px;">
			<font size="2">
			A brief history... <br>
			Bellas is a family-owned restaurant that first opened its doors in 1996 and started its humble beginnings as a
			coffee shop; serving pastries, sandwiches, and locally made candy.  Then,
			upon the youngest son's graduation from the Culinary Institute of America, the family
			decided to transition its business to a full service American restaurant.
			This transition was met with great success and Bellas has since become a destination
			for locals and travellers alike.
			</font></p></center>
		<br><br>
		<hr>
		<p><center><font size="2">234 Whispering Pines Lane, Chippewa Falls, WI, 54729
		<br>
		(555) 555-5555</font></center>
		</p>
		<hr>
	</body>
</html>