﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phonebook.Models
{
    public class AddContactVM
    {
        public Contact Contact { get; set; }
        public List<SelectListItem> FriendRating { get; set; }
        public List<SelectListItem> StarPower { get; set; }

        public AddContactVM()
        {
            FriendRating = new List<SelectListItem>
            {
                new SelectListItem {Text="Acquaintence", Value="Acquaintence" },
                new SelectListItem {Text="Friends", Value="Friends" },
                new SelectListItem {Text="Pretty Tight", Value="Pretty Tight" },
                new SelectListItem {Text="Besties", Value="Besties" },
            };

            StarPower = new List<SelectListItem>
            {
                new SelectListItem {Text="0", Value="0" },
                new SelectListItem {Text="1", Value="1" },
                new SelectListItem {Text="2", Value="2" },
                new SelectListItem {Text="3", Value="3" },
                new SelectListItem {Text="4", Value="4" },
                new SelectListItem {Text="5", Value="5" },
            };
        }
    }
}