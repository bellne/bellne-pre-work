﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phonebook.Models
{
    public class ContactRepository
    {
        private static List<Contact> _contacts { get; set; }

        static ContactRepository()
        {
            _contacts = new List<Contact>
            {
                new Contact {FirstName="Miranda", LastName="Lambert", PhoneNumber="8087548866", Email="ml@gmail.com", id=1, FriendRating="Acquaintence", StarPower=1 },
                new Contact {FirstName="Selena", LastName="Gomez", PhoneNumber="2126548899", Email="sg@gmail.com", id=2, FriendRating="Acquaintence", StarPower=1 },
                 new Contact {FirstName="Taylor", LastName="Swift", PhoneNumber="6542159945", Email="ts@gmail.com", id=2, FriendRating="Bff", StarPower=10 }
            };
        }

        public static List<Contact> GetAllContacts()
        {
            return _contacts;
        }

        public static Contact Get(int Id)
        {
            return _contacts.FirstOrDefault(c => c.id == Id);
        }

        public static void InsertContact(Contact newContact)
        {
            if (_contacts.Count() == 0)
            {
                newContact.id = 1;
                _contacts.Add(newContact);
            }
            else
            {
                newContact.id = _contacts.Max(m=>m.id) + 1;
                _contacts.Add(newContact);
            }
        }

        public static void DeleteContact(int id)
        {
            _contacts.RemoveAll(x => x.id == id);
        }

        public static void EditContact(Contact editedcontact)
        {
            _contacts.RemoveAll(x => x.id == editedcontact.id);
            _contacts.Add(editedcontact);
        }
    }
}