﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookDemo.BLL;
using AddressBookDemo.Models;

namespace AddressBookDemo
{
    public class AddressBookUi
    {
        private AddressBookManager _manager = new AddressBookManager();
        public void ShowMenu()
        {
            Console.Clear();
            string choice;
            int iChoice = 0;
            bool isValidChoice = false;
            do
            {
                Console.WriteLine("Please select a menu choice\n");
                Console.WriteLine("1. Add Entry\n");
                Console.WriteLine("2. Edit Entry\n");
                Console.WriteLine("3. Delete Entry\n");

                choice = Console.ReadLine();
                bool isANmuber = int.TryParse(choice, out iChoice);


                if(isANmuber && iChoice < 4 && iChoice > 0)
                {
                    isValidChoice = true;
                }
                else
                {
                    Console.WriteLine("Please try again, enter a number 1, 2, or 3.\n");
                }
            } while (!isValidChoice);

            switch (iChoice)
            {
                case 1: AddEntryPrompt();
                    break;
                case 2: EditEntryPrompt();
                    break;
                case 3: DeleteEntryPrompt();
                    break;
                default:
                    throw new Exception("This will never happen (famous last comments)");
            }

            AddEntryPrompt();
        }

        public void DeleteEntryPrompt()
        {
            Console.Clear();
            var listOfContacts = _manager.GetAllContacts();
            foreach(Contact contact in listOfContacts.OrderBy(x => x.LastName))
            {
                Console.WriteLine("{0}. {1}, {2}", contact.Id, contact.LastName, contact.FirstName);
            }
            Console.Write("Please select a contact to delete: ");
            // should probably validate this ID
            var selectId = int.Parse(Console.ReadLine());

            Contact selectedContact = listOfContacts.SingleOrDefault(x => selectId == x.Id);

            Console.Clear();
            Console.WriteLine("Are you sure you want to delete {0} {1}?",  
                selectedContact.FirstName, selectedContact.LastName);
            string response = Console.ReadLine().ToLower();
            if(response == "y")
            {
                _manager.DeleteEntryInAddressBook(selectedContact);
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Contact successfully deleted");
                Console.ResetColor();
                Console.WriteLine("Press enter to return to main menu");
                Console.ReadLine();
                ShowMenu();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You decided NOT to delete this contact");
                Console.ResetColor();
                Console.WriteLine("Press enter to return to main menu");
                Console.ReadLine();
                ShowMenu();
            }
           
        }

        public void EditEntryPrompt()
        {
            Console.Clear();
            var listOfContacts = _manager.GetAllContacts();
            foreach(Contact contact in listOfContacts.OrderBy(x => x.LastName))
            {
                Console.WriteLine("{0}. {1}, {2}", contact.Id, contact.LastName, contact.FirstName);
            }
            Console.Write("Please select a contact to edit: ");
            // should probably validate this ID
            var selectId = int.Parse(Console.ReadLine());

            // singleordefault makes sure there isn't two instances of the same Id
            Contact selectedContact = listOfContacts.SingleOrDefault(x => selectId == x.Id);

            Console.Clear();
            Console.WriteLine("Select a field to edit: ");
            Console.WriteLine();
            Console.WriteLine("1. First Name");
            Console.WriteLine("2. Last Name");
            Console.WriteLine("3. Phone Number");
            Console.WriteLine("4. Email");
            Console.WriteLine("5. Address");
            Console.WriteLine();

            //should validate this too
            var selectedField = int.Parse(Console.ReadLine());

            //should validate that it's a number between 1 and 5
            switch (selectedField)
            {
                case 1:
                    Console.Clear();
                    Console.Write("Enter the new First Name: ");
                    selectedContact.FirstName = Console.ReadLine();
                    break;
                case 2:
                    Console.Clear();
                    Console.Write("Enter the new Last Name: ");
                    selectedContact.FirstName = Console.ReadLine();
                    break;
                case 3:
                    Console.Clear();
                    Console.Write("Enter the new Phone Number: ");
                    selectedContact.FirstName = Console.ReadLine();
                    break;
                case 4:
                    Console.Clear();
                    Console.Write("Enter the new Email: ");
                    selectedContact.FirstName = Console.ReadLine();
                    break;
                case 5:
                    Console.Clear();
                    Console.Write("Enter the new Address: ");
                    selectedContact.FirstName = Console.ReadLine();
                    break;
            }

            _manager.EditEntryInAddressBook(selectedContact);
            ShowMenu();
        }

        public void AddEntryPrompt()
        {
            Console.Clear();
            Console.Write("First name: ");
            var firstName = Console.ReadLine();
            Console.Write("Last name: ");
            var lastName = Console.ReadLine();
            Console.Write("Phone Number: ");
            var phoneNumber = Console.ReadLine();
            Console.Write("Email: ");
            var email = Console.ReadLine();
            Console.Write("Address: ");
            var address = Console.ReadLine();

            Contact newContact = new Contact()
            {
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phoneNumber,
                Email = email,
                Address = address
            };

            _manager.AddEntryToAddressBook(newContact);
            ShowMenu();
        }
    }
}
