﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookDemo.Data.Repositories;
using AddressBookDemo.Data;
using AddressBookDemo.Models;

namespace AddressBookDemo.BLL
{
    public class AddressBookManager
    {
        private IAddressBookRepository _addyRepo = new TestAddressBookRepository();

        public void AddEntryToAddressBook(Contact contact)
        {
            if(contact.LastName.ToLower() != "johnson")
            {
                _addyRepo.AddEntry(contact);
            }
        }

        public void EditEntryInAddressBook(Contact contact)
        {
            if (contact.LastName.ToLower() != "johnson")
            {
                _addyRepo.EditEntry(contact);
            }
            else
            {
                throw new Exception("My boss really hates anyone with the last name Johnson.");
            }
        }

        public void DeleteEntryInAddressBook(Contact contact)
        {
            if (contact.LastName.ToLower() != "johnson")
            {
                _addyRepo.DeleteEntry(contact);
            }
            else
            {
                throw new Exception("My boss really hates anyone with the last name Johnson.");
            }
        }

        public List<Contact> GetAllContacts()
        {
            return _addyRepo.GetAllContacts();
        }
    }
}
