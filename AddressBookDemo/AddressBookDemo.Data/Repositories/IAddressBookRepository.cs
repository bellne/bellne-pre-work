﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookDemo.Models;

namespace AddressBookDemo.Data
{
    public interface IAddressBookRepository
    {
        void AddEntry(Contact newContact);
        void EditEntry(Contact editedContact);
        void DeleteEntry(Contact toDeleteContact);
        List<Contact> GetAllContacts();
        Contact GetContact(int id);
    }
}
