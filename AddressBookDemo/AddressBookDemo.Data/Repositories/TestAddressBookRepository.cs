﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookDemo.Models;

namespace AddressBookDemo.Data.Repositories
{
    public class TestAddressBookRepository : IAddressBookRepository
    {
        private AddressBook _testAddyBook = new AddressBook();

        public void AddEntry(Contact newContact)
        {
            if(_testAddyBook.Contacts.Count() > 0)
            {
                newContact.Id = _testAddyBook.Contacts.Select(x => x.Id).Max() + 1;
            }
            else
            {
                newContact.Id = 1;
            }
            _testAddyBook.Contacts.Add(newContact);
        }

        public void EditEntry(Contact editedContact)
        {
            var contactToEdit = GetContact(editedContact.Id);
            contactToEdit.FirstName = editedContact.FirstName;
            contactToEdit.LastName = editedContact.LastName;
            contactToEdit.PhoneNumber = editedContact.PhoneNumber;
            contactToEdit.Email = editedContact.Email;
            contactToEdit.Address = contactToEdit.Address;
        }

        public List<Contact> GetAllContacts()
        {
            return _testAddyBook.Contacts;
        }

        public Contact GetContact(int id)
        {
            return _testAddyBook.Contacts.SingleOrDefault(x => x.Id == id);
        }

        public void DeleteEntry(Contact toDeleteContact)
        {
            _testAddyBook.Contacts.Remove(toDeleteContact);            
        }
    }
}
