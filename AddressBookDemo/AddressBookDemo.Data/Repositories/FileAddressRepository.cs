﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookDemo.Models;
using System.IO;

namespace AddressBookDemo.Data.Repositories
{
    class FileAddressRepository : IAddressBookRepository
    {
        private string _filename = "AddressBook.csv";

        public void AddEntry(Contact newContact)
        {
           
        }

        public void DeleteEntry(Contact toDeleteContact)
        {
            throw new NotImplementedException();
        }

        public void EditEntry(Contact editedContact)
        {
            throw new NotImplementedException();
        }

        public List<Contact> GetAllContacts()
        {
            var allLines = File.ReadAllLines(_filename);
            List<Contact> resultSet = new List<Contact>();

            for (int i = 1; i < allLines.Count(); i++)
            {
                var fields = allLines[i].Split(',');
                Contact existingContact = new Contact()
                {
                    Id = int.Parse(fields[0]),
                    FirstName = fields[1],
                    LastName = fields[2],
                    PhoneNumber = fields[3],
                    Email = fields[4],
                    Address = fields[5]
                };
                resultSet.Add(existingContact);
            }
            return resultSet;
        }

        public Contact GetContact(int id)
        {
            throw new NotImplementedException();
        }
    }
}
