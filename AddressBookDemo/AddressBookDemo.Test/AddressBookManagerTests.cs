﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AddressBookDemo.Models;
using AddressBookDemo.BLL;

namespace AddressBookDemo.Test
{
    [TestFixture]
    public class AddressBookManagerTests
    {
        [TestCase("johnson", 0)]
        [TestCase("anderson", 1)]
        [TestCase("Franky", 1)]
        public void TestAddEntryBizRuleNoJohnsons(string lastName, int expectedNumberOfEntries)
        {
            // Arrange
            AddressBookManager manager = new AddressBookManager();
            Contact contactToAdd = new Contact()
            {
                LastName = lastName
            };
            // Act
            manager.AddEntryToAddressBook(contactToAdd);

            int actualNumberOfEntries = manager.GetAllContacts().Count();

            // Assert
            Assert.AreEqual(expectedNumberOfEntries, actualNumberOfEntries);
        }
    }
}
