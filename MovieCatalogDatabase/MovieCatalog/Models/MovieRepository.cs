﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MovieCatalog.Models
{
    public class MovieRepository
    {
        private static List<Movie> _movies;

        //static MovieRepository()
        //{
        //    _movies = new List<Movie>
        //    {
        //        new Movie {MovieId=1, Title="Ghostbusters", Rating="PG-13", Stars=4 },
        //        new Movie {MovieId=2, Title="Star Wars", Rating="PG", Stars=5 },
        //        new Movie {MovieId=3, Title="Aristocats", Rating="G", Stars=2 }
        //    };
        //}

        //public static List<Movie> GetAll()
        //{
        //    return _movies;
        //}


        public static List<Movie> GetAll()
        {
            _movies = new List<Movie>();
            
            string connectionString = ConfigurationManager
                                    .ConnectionStrings["MovieCatalogDatabase"]
                                    .ConnectionString; 

            // create connection with connection string
            // using try to automatically close it
            
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    // Create a command
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandText = "SELECT* FROM movierepository";
                    sqlCommand.Connection = sqlConnection;

                    sqlConnection.Open(); // must have open connection to query

                    // call ExecuteReader to have the command create a 
                    // data reader, this executes the SQL Statement
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        //Read() returns false when there is no more data
                        while (sqlDataReader.Read())
                        {
                            Movie movie = new Movie();
                            movie.MovieId = Int32.Parse(sqlDataReader["MovieID"].ToString());
                            movie.Title = sqlDataReader["MovieTitle"].ToString();
                            movie.Rating = sqlDataReader["Rating"].ToString();
                            movie.Stars = Int32.Parse(sqlDataReader["Stars"].ToString());
                            _movies.Add(movie);
                        }

                        sqlConnection.Close();
                    }

                }
            return _movies;          
        }

        public static Movie Get(int id)
        {
            _movies = new List<Movie>();
            Movie movie = new Movie();
            string connectionString = ConfigurationManager
                                    .ConnectionStrings["MovieCatalogDatabase"]
                                    .ConnectionString;

            // create connection with connection string
            // using try to automatically close it

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                // Create a command
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "SELECT* FROM movierepository";
                sqlCommand.Connection = sqlConnection;

                sqlConnection.Open(); // must have open connection to query

                // call ExecuteReader to have the command create a 
                // data reader, this executes the SQL Statement
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    //Read() returns false when there is no more data
                    while (sqlDataReader.Read())
                    {
                        movie.MovieId = Int32.Parse(sqlDataReader["MovieID"].ToString());
                        movie.Title = sqlDataReader["MovieTitle"].ToString();
                        movie.Rating = sqlDataReader["Rating"].ToString();
                        movie.Stars = Int32.Parse(sqlDataReader["Stars"].ToString());
                        _movies.Add(movie);
                    }

                    sqlConnection.Close();
                }

            }

            return _movies.FirstOrDefault(m => m.MovieId == id);
        }

        public static void Insert(Movie movie)
        {


            movie.MovieId = _movies.Max(m => m.MovieId) + 1;
            _movies.Add(movie);
        }

        public static void Delete(int id)
        {
            _movies.RemoveAll(m => m.MovieId == id);
        }

        public static void Edit(Movie movies)
        {
            int @MovieID = movies.MovieId;
            string @MovieTitle = movies.Title.ToString();
            string @Rating = movies.Rating.ToString();
            int @Stars = Int32.Parse(movies.Stars.ToString());
            _movies = new List<Movie>();
            Movie movie = new Movie();
            string connectionString = ConfigurationManager
                                    .ConnectionStrings["MovieCatalogDatabase"]
                                    .ConnectionString;

            // create connection with connection string
            // using try to automatically close it

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                // Create a command
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "delete from Movierepository where MovieID = " + @MovieID;
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Connection = sqlConnection;


                sqlConnection.Open(); // must have open connection to query
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "insert into movierepository (MovieTitle, Rating, Stars) values(@MovieTitle, @Rating, @Stars)";
                sqlCommand.CommandType = System.Data.CommandType.Text;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Parameters.AddWithValue("@MovieTitle", movies.Title.ToString());
                sqlCommand.Parameters.AddWithValue("@Rating", movies.Rating.ToString());
                sqlCommand.Parameters.AddWithValue("@Stars", movies.Stars);

                sqlConnection.Open(); // must have open connection to query
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            
            //_movies.RemoveAll(m => m.MovieId == movies.MovieId);
            //_movies.Add(movie);
        }
    }
}