CREATE Table MovieRepository
(
	[MovieID] [int] IDENTITY(1,1) not null primary key,
	[MovieTitle] [varchar](30) not null,
	[Rating] [varchar](5) null,
	[Stars] [int] null
)

insert into Movierepository (MovieTitle, Rating, Stars)
	values('Ghostbusters', 'PG-13', 4 ),
        ('Star Wars', 'PG', 5 ),
        ('Aristocats', 'G', 2 );

delete from Movierepository
where MovieID = 4

insert into movierepository (MovieTitle, Rating, Stars)
	values('Aristocats', 'G', 2 );

update movierepository
set movieid = 3
where movieid = 4;

select*
from movierepository
