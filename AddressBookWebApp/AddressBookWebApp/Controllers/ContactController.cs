﻿using AddressBookWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AddressBookWebApp.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Add()
        {
            var model = new AddContactVM();
            model.Contact = new Contact();

            return View(model);
        }

        [HttpPost]
        public ActionResult PostContact(AddContactVM newContact)
        {
            AddressBook.Insert(newContact.Contact);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Delete()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            var model = AddressBook.Get(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Contact contact)
        {
            AddressBook.Edit(contact);

            return RedirectToAction("Index", "Home");
        }
    }
}