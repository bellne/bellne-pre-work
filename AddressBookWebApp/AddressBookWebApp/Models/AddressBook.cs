﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AddressBookWebApp.Models
{
    public static class AddressBook
    {
        private static List<Contact> _contacts { get; set; }

        static AddressBook()
        {
            _contacts = new List<Contact>
            {
                new Contact { Id = 1, FirstName = "Taylor", LastName = "Swift", Address = "2244 Style Dr", Email = "ts@gmail.com", PhoneNumber = "7155556454"},
                new Contact { Id = 2, FirstName = "Selena", LastName = "Gomez", Address = "2244 Style Dr", Email = "sg@gmail.com", PhoneNumber = "8082239865"},
                new Contact { Id = 3, FirstName = "Miranda", LastName = "Lambert", Address = "3322 Nashville Ave", Email = "mr@gmail.com", PhoneNumber = "1214987894"}
            };
        }

        public static List<Contact> GetAll()
        {
            return _contacts;
        }

        public static Contact Get (int id)
        {
            return _contacts.FirstOrDefault(c => c.Id == id);
        }

        public static void Insert (Contact contact)
        {
            contact.Id = _contacts.Max(c => c.Id) + 1;
            _contacts.Add(contact);
        }

        public static void Delete (int id)
        {
            _contacts.RemoveAll(c => c.Id == id);
        }

        public static void Edit (Contact contact)
        {
            _contacts.RemoveAll(c => c.Id == contact.Id);
            _contacts.Add(contact);
        }
    }
}