﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AddressBookWebApp.Models
{
    public class AddContactVM
    {
        public Contact Contact { get; set; }
    }
}