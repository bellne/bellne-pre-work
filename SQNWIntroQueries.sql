USE Northwind;
GO

SELECT CompanyName, City, [Country] 
FROM Customers
WHERE Country = 'USA'

SELECT CompanyName, City, [Country] 
FROM Customers
WHERE Country != 'USA'

SELECT CompanyName, City, [Country], Region
FROM Customers
WHERE Country = 'USA' AND Region ='OR'

SELECT CompanyName, City, [Country] 
FROM Customers
WHERE Country IN ('USA', 'UK')

SELECT CompanyName, City, [Country] 
FROM Customers
WHERE Country NOT IN ('USA', 'UK')

