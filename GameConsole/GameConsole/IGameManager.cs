﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameConsole
{
    interface IGameManager
    {
        void Play();
        string GetGameName();
    }
}
