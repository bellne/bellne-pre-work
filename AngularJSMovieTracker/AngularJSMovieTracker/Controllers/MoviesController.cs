﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSMovieTracker.Models;

namespace AngularJSMovieTracker.Controllers
{
    public class MoviesController : ApiController
    {
        private static readonly MovieRepository Repo = new MovieRepository();

        // GET api/<controller>
        public IEnumerable<Movie> Get()
        {
            return Repo.GetAll();
        }

        // GET api/<controller>/5
        public Movie Get(int id)
        {
            Movie item = Repo.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return item;
        }

        // POST api/<controller>
        public HttpResponseMessage Post(Movie movie)
        {
            movie = Repo.Add(movie);
            var response = Request.CreateResponse(HttpStatusCode.Created, movie);

            string uri = Url.Link("DefaultApi", new { id = movie.MovieId });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        // PUT api/<controller>/5
        public void Put(int id, Movie movie)
        {
            if (Repo.Get(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            movie.MovieId = id;
            Repo.Edit(movie);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Movie movie = Repo.Get(id);
            if (movie == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            Repo.Delete(id);
        }
    }
}