﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

namespace AngularJSMovieTracker.Models
{
    public class MovieRepository
    {
        private List<Movie> _movies;
        private int _nextId = 4;
        public MovieRepository()
        {
            _movies = new List<Movie>
                {
                    new Movie { Title="GhostBusters", MovieId=1, ReleaseYear=1984, Cast = new List<string>{"Bill Muray", "Dan Aykroyd"}},
                    new Movie { Title="Trading Places", MovieId=2, ReleaseYear=1983, Cast = new List<string>{"Eddie Murphy", "Dan Aykroyd"}},
                    new Movie { Title="The Blues Brothers", MovieId=3, ReleaseYear=1980, Cast = new List<string>{"John Belushi", "Dan Aykroyd"}}
                };
        }

        public List<Movie> GetAll()
        {
            return _movies;
        }

        public Movie Get(int id)
        {
            return _movies.FirstOrDefault(m => m.MovieId == id);
        }

        public Movie Add(Movie movie)
        {
            movie.MovieId = _nextId++;
            _movies.Add(movie);

            return movie;
        }

        public bool Edit(Movie editedMovie)
        {
            _movies.RemoveAll(m => m.MovieId == editedMovie.MovieId);
            _movies.Add(editedMovie);
            return true;
        }

        public void Delete(int id)
        {
            _movies.RemoveAll(m => m.MovieId == id);
        }
    }
}