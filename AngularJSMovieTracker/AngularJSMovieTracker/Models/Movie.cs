﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSMovieTracker.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
        public List<string> Cast { get; set; }

        public Movie()
        {
            Cast = new List<string>();
        }
    }
}