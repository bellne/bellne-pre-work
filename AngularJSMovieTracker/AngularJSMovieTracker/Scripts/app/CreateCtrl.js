﻿/// <reference path="movieModule.js" />

app.controller('CreateCtrl', function ($scope, $location, movieFactory) {
    $scope.showDelete = false;

    $scope.save = function () {
        $scope.movie.Cast = $scope.movie.CastList.split(',');
        movieFactory.post($scope.movie)
            .success(function () {
                $location.path('/');
            })
            .error(function (data, status) {
                alert('Something went wrong!  Status: ' + status);
            });
    };
});