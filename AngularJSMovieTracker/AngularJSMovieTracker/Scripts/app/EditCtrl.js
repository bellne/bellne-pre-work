﻿/// <reference path="movieModule.js" />

app.controller('EditCtrl', function ($scope, $location, $routeParams, movieFactory) {
    movieFactory.getMovieById($routeParams.movieId)
        .success(function (movie) {
            $scope.movie = movie;
            $scope.movie.CastList = movie.Cast.toString(', ');
        })
        .error(function (data, status) {
            alert('Something went wrong!  Status: ' + status);
        });

    $scope.showDelete = true;

    $scope.delete = function () {
        movieFactory.deleteMovie($routeParams.movieId)
            .success(function () {
                $location.path('/');
            })
            .error(function (data, status) {
                alert('Something went wrong!  Status: ' + status);
            });
    };

    $scope.save = function () {
        $scope.movie.Cast = $scope.movie.CastList.split(',');
        movieFactory.put($routeParams.movieId, $scope.movie)
            .success(function () {
                $location.path('/');
            })
            .error(function (data, status) {
                alert('Something went wrong!  Status: ' + status);
            });
    };
});