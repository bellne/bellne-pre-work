﻿/// <reference path="movieModule.js" />

app.controller('ListCtrl', function ($scope, movieFactory) {
    movieFactory.getMovies()
        .success(function (movies) {
            $scope.movies = movies;
        })
        .error(function (data, status) {
            alert('Something went wrong!  Status: ' + status);
        });
});