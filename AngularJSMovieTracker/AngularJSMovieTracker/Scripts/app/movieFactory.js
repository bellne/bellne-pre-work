﻿/// <reference path="movieModule.js" />
app.factory('movieFactory', function ($http) {
    var factory = {};
    var url = '/api/movies/';

    factory.getMovies = function () {
        return $http.get(url);
    };

    factory.getMovieById = function (id) {
        return $http.get(url + '/' + id);
    };

    factory.deleteMovie = function (id) {
        return $http.delete(url + '/' + id);
    };

    factory.post = function (movie) {
        return $http.post(url, angular.toJson(movie));
    };

    factory.put = function (id, movie) {
        return $http.put(url + '/' + id, movie);
    };

    return factory;
});