﻿/// <reference path="../angular.min.js" />
var app = angular.module('movies', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'ListCtrl',
            templateUrl: '/Routes/list.html'
        })
        .when('/edit/:movieId', {
            controller: 'EditCtrl',
            templateUrl: '/Routes/detail.html'
        })
        .when('/new', {
            controller: 'CreateCtrl',
            templateUrl: '/Routes/detail.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});