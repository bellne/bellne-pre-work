﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays_Lab___Roll_die_100_times
{
    class Program
    {
        static void Main(string[] args)
        {
            int rolledAOne = 0;
            int rolledATwo = 0;
            int rolledAThree = 0;
            int rolledAFour = 0;
            int rolledAFive = 0;
            int rolledASix = 0;

            Random dieRoll = new Random();

            for (int i = 1; i < 101; i++)
            {                
                int rollResult = dieRoll.Next(1, 7);               

                switch (rollResult)
                {
                     case 1:                       
                        rolledAOne += 1;
                        break;
                    case 2:
                        rolledATwo += 1;
                        break;
                    case 3:
                        rolledAThree += 1;
                        break;
                    case 4:
                        rolledAFour += 1;
                        break;
                    case 5:
                        rolledAFive += 1;
                        break;
                    case 6:
                        rolledASix += 1;
                        break;
                     default:
                        break;
                        
                }
                Console.WriteLine("Roll {0}: {1}", i, rollResult);                
            }
            Console.WriteLine("\nResults:");
            Console.Write("You rolled a: \n");
            Console.WriteLine("   1: {0} times", rolledAOne);
            Console.WriteLine("   2: {0} times", rolledATwo);
            Console.WriteLine("   3: {0} times", rolledAThree);
            Console.WriteLine("   4: {0} times", rolledAFour);
            Console.WriteLine("   5: {0} times", rolledAFive);
            Console.WriteLine("   6: {0} times", rolledASix);
            Console.ReadLine();
        }
    }
}
