﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalcKata
{
    public class Program
    {

        static void Main()
        {
          
        }

        // sample input: "1,2,3,24,5"
        public static int Add(string numbers)
        {
            string[] theNumbers = numbers.Split('\n', ',');

            // theNumbers is an array of string with the following contents:
            // theNumbers[0] "1"
            // theNumbers[1] "2"
            // theNumbers[2] "3"
            // theNumbers[3] "24"
            // etc, etc

            int realValue = 0;
            // what is theNumbers.Length = 0
            for (int i = 0; i < theNumbers.Length; i++)
            {
                int num = 0;
                if (int.TryParse(theNumbers[i], out num))
                {
                    realValue += num;
                } 
                //int sum;
                //sum = realValue;

            }
            return realValue;
        }
    }
}
