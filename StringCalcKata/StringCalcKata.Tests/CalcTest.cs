﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StringCalcKata;

namespace StringCalcKata.Tests
{
    [TestFixture]
    public class CalcTest
    {
        [Test]
        public void TestCalculatorAdd()
        {
            // Arrange
            string nums = "";
            int expectedAmount = 0;

            // Act
            int actualAmount = Program.Add(nums);

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [Test]
        public void TestAddOneNumber()
        {
            //Arrange
            string nums = "1";
            int expectedAmount = 1;

            // Act
            int actualAmount = Program.Add(nums);

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [Test]
        public void ActuallyAddSomeNumbers()
        {
            // Arrange
            string nums = "1,2,3,24,5";
            int expectedAmount = 35;

            // Act
            int actualAmount = Program.Add(nums);

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [Test]
        public void AddWithDifferentCharacters()
        {
            // Arrange
            string nums = "1\n2\n3\n24\n5";
            int expectedAmount = 35;

            // Act
            int actualAmount = Program.Add(nums);

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount);
        }
    }
}
