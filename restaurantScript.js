function validate()
{
	var dropdown = document.getElementById("reasonList");
	//var addInfo = document.getElementById("addInfo");
	
	if (document.contactUs.myName.value == "" || (document.contactUs.myEmail.value == "" && document.contactUs.myPhone.value == ""))
	{
		alert("Please enter name and one form of contact information (email or phone)");
		return false;
	}
	
	else if (dropdown.value == "other" /*&& document.contactUs.additionalInfo.value == ""*/)
	{
		alert("Please describe the reason for your inquiry in the 'Additional Information' section");
		return false;
	}
	
	else if (!document.getElementById("MONDAY").checked && !document.getElementById("TUESDAY").checked && !document.getElementById("WEDNESDAY").checked && !document.getElementById("THURSDAY").checked
			&& !document.getElementById("MONDAY").checked)
	{
		alert("Must check one box");
		return false;
	}
	
	else
	{
		return 1;
	}
}