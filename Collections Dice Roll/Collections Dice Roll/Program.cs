﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections_Dice_Roll
{
    class Program
    {
        static void Main(string[] args)
        {
            Random die = new Random();
            List<int> result = new List<int>();
            int two = 0, three = 0, four = 0, five = 0, six = 0, seven = 0, eight = 0, nine = 0,
                ten = 0, eleven = 0, twelve = 0;
            int roll = 1;
            int totalRoll = 0;
            int[] numberCount = new int[11];

            for (int i = 1; i < 101; i++)
            {
                int rollValue1 = die.Next(1, 7);
                int rollValue2 = die.Next(1, 7);
                totalRoll = rollValue1 + rollValue2;

                result.Add(totalRoll);

                switch (totalRoll)
                {
                    case 2:
                        two += 1;
                        break;
                    case 3:
                        three += 1;
                        break;
                    case 4:
                        four += 1;
                        break;
                    case 5:
                        five += 1;
                        break;
                    case 6:
                        six += 1;
                        break;
                    case 7:
                        seven += 1;
                        break;
                    case 8:
                        eight += 1;
                        break;
                    case 9:
                        nine += 1;
                        break;
                    case 10:
                        ten += 1;
                        break;
                    case 11:
                        eleven += 1;
                        break;
                    case 12:
                        twelve += 1;
                        break;
                    default:
                        break;
                }
            }


                foreach (var totals in result)
                {
                    Console.WriteLine("Roll {0}: {1}", roll, totals);
                    roll++;
                }

                Console.WriteLine("\nSummary of how many times each number was rolled:");
            Console.WriteLine("Two: {0}\nThree: {1}\nFour: {2}\nFive: {3}\nSix: {4}" +
                "\nSeven: {5}\nEight: {6}\nNine: {7}\nTen: {8}\nEleven: {9}\nTwelve: {10}",
                two, three, four, five, six, seven, eight, nine, ten, eleven, twelve);
            Console.ReadLine();
            
        }
          
    }
}
