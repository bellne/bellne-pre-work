﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models
{
    public class CustomerOrderLog
    {
        public List<CustomerOrder> CustomerOrder { get; set; }

        public void OrderList()
        {
            CustomerOrder = new List<CustomerOrder>();
        }
    }
}
