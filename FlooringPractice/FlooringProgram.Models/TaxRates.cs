﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models
{
    public class TaxRates
    {
        public List<TaxRate> TaxRateList { get; set; }

        public TaxRates()
        {
            TaxRateList = new List<TaxRate>();
        }
    }
}
