﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models
{
    public class CustomerOrder
    {
        public string OrderNumber { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StateAbbreviation { get; set; }
        public decimal TaxRate { get; set; }
        public string ProductType { get; set; }
        public int Area { get; set; }
        public decimal CostPSF { get; set; }
        public decimal LaborCostPSF { get; set; }
        public decimal MaterialCost { get; set; }
        public decimal LaborCost { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
    }
}
