﻿$(document).ready(function () {
    $('#rsvpForm').validate({
        rules: {
            Name: {
                required:true
            },
            Email: {
                required: true,
                email: true
            },
            Phone: {
                required: true
            },
            FavoriteGame: {
                required: true,
                //i think the names of these can be found on a jquery website somewhere
                minlength: 3
            },
            WillAttend: {
                required:true
            }
        },
        messages: {
            Name: "Enter your name",
            FavoriteGame: {
                required: "Tell us your favorite game",
                minlength: $.validator.format("I don't know of any game with less than {0} characters...")
            }
        }
    });
});