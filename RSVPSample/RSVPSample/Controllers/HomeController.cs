﻿using RSVPSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSVPSample.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Home/RegistrationForm
        public ActionResult RegistrationForm()
        {
            var model = new RsvpResponse();
            return View(model);
        }

        // POST: /Home/RegistrationForm
        [HttpPost]
        public ActionResult RegistrationForm(RsvpResponse model)
        {
            if (ModelState.IsValid)
            {
                return View("RegistrationConfirmation", model);
            }

            return View(model);
        }

        // GET: /Home/RegistrationForm2
        public ActionResult RegistrationForm2()
        {
            var model = new RsvpResponse();
            return View(model);
        }

        // POST: /Home/RegistrationForm
        [HttpPost]
        public ActionResult RegistrationForm2(RsvpResponse model)
        {
            if (ModelState.IsValid)
            {
                return View("RegistrationConfirmation", model);
            }

            return View(model);
        }
    }
}