﻿CREATE TABLE [dbo].[Tag] (
    [PostTagID]      INT          IDENTITY (1, 1) NOT NULL,
    [TagDescription] VARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([PostTagID] ASC)
);



