﻿CREATE TABLE [dbo].[Status] (
    [PostStatusID]      INT          IDENTITY (1, 1) NOT NULL,
    [StatusDescription] VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([PostStatusID] ASC)
);



