﻿CREATE TABLE [dbo].[PostTag] (
    [PostID]    INT NOT NULL,
    [PostTagID] INT NOT NULL,
    CONSTRAINT [FK_PostTable_PostID] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Post] ([PostID]),
    CONSTRAINT [FK_PostTable_PostTagID] FOREIGN KEY ([PostTagID]) REFERENCES [dbo].[Tag] ([PostTagID])
);



