﻿CREATE TABLE [dbo].[Post] (
    [PostID]        INT           IDENTITY (1, 1) NOT NULL,
    [PostTitle]     VARCHAR (50)  NOT NULL,
    [PostBody]      VARCHAR (500) NOT NULL,
    [DateCreated]   DATE          NOT NULL,
    [DatePublished] DATE          NULL,
    [IsStatic]      BIT           NOT NULL,
    [UserId]        INT           NOT NULL,
    [PostStatusId]  INT           NOT NULL,
    [StaticPageId]  INT           NULL,
    PRIMARY KEY CLUSTERED ([PostID] ASC),
    FOREIGN KEY ([StaticPageId]) REFERENCES [dbo].[StaticPage] ([StaticPageId]),
    CONSTRAINT [FK_Post_PostStatus] FOREIGN KEY ([PostStatusID]) REFERENCES [dbo].[Status] ([PostStatusId]),
    CONSTRAINT [FK_Post_UserID] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);





