﻿CREATE TABLE [dbo].[PostCategory] (
    [PostID]     INT NOT NULL,
    [CategoryID] INT NOT NULL,
    CONSTRAINT [FK_PostCategory_CategoryID] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID]),
    CONSTRAINT [FK_PostCategory_PostID] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Post] ([PostID])
);

