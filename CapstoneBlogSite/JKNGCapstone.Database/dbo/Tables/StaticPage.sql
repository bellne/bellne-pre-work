﻿CREATE TABLE [dbo].[StaticPage] (
    [StaticPageId]          INT          IDENTITY (1, 1) NOT NULL,
    [StaticPageDescription] VARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([StaticPageId] ASC)
);

