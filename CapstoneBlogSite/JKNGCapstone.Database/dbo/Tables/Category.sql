﻿CREATE TABLE [dbo].[Category] (
    [CategoryID]          INT          IDENTITY (1, 1) NOT NULL,
    [CategoryDescription] VARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);

