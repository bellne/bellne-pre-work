﻿
  CREATE PROCEDURE [dbo].[GetStaticPageById]
  (
	@PostId int
  ) AS

  select PostId, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, post.userid, post.poststatusid, Username, StatusDescription
  from post
	left join [status]
	on post.PostStatusId = [status].PostStatusId
	left join [user]
	on post.UserId = [user].UserID
	where postid = @PostId