﻿CREATE PROCEDURE [dbo].[PublishPost]
(
	@PostId int,
	@DatePublished date
)AS

UPDATE Post
SET PostStatusId = 3, DatePublished = @DatePublished
WHERE PostID = @PostId