﻿CREATE PROCEDURE DeleteSingleStaticPage
(
	@PostID int
) AS

UPDATE Post
set PostStatusId = 5
WHERE PostID = @PostID