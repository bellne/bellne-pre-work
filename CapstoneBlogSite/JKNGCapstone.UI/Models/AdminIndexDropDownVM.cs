﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.UI.Models
{
    public class AdminIndexDropDownVM
    {
        public List<SelectListItem> DropDownList { get; set; }

        public AdminIndexDropDownVM()
        {
            DropDownList = new List<SelectListItem>
            {
                new SelectListItem { Text = "Post Index", Value = "1" },
                new SelectListItem { Text = "User Management", Value = "2" }
            };
            


        }
    }
}