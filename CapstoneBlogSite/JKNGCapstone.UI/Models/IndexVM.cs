﻿using JKNGCapstone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKNGCapstone.UI.Models
{
    public class IndexVM
    {
        public IndexVM() { }
        public List<Post> Posts { get; set; }
        public int Count { get; set; }
    }
}