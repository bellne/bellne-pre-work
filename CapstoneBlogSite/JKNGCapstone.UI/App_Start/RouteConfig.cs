﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace JKNGCapstone.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("SarahsSpirits", "SarahsSpirits/{*pathinfo}", new { controller = "Blog", action = "Index", id = 1 });
            routes.MapRoute("BlogDashboard", "BlogDashboard/{*pathinfo}", new { controller = "Blog", action = "AdminIndex" });
            routes.MapRoute("StaticPageDashboard", "StaticPageDashboard/{*pathinfo}", new { controller = "Blog", action = "StaticIndex", id = 1 });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Blog", action = "Index", id = 1 }
            );
        }
    }
}
