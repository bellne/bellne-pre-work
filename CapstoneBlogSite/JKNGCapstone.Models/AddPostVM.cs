﻿using Dapper;
using JKNGCapstone.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace JKNGCapstone.Models
{
    public class AddPostVM
    {
        public Post Post { get; set; }
        public List<SelectListItem> Tag { get; set; }
        public List<SelectListItem> Category { get; set; }

        public AddPostVM()
        {
            Post = new Post { PostTitle = "Title", PostBody = "Type post here" };
            List<PostTag> tags = new List<PostTag>();     
            List<Category> categories = new List<Category>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string sqlQuery = "SELECT PostTagID, TagDescription FROM Tag";
                tags = sqlConnection.Query<PostTag>(sqlQuery).AsList();
            }

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string sqlQuery = "SELECT CategoryID, CategoryDescription FROM Category";
                categories = sqlConnection.Query<Category>(sqlQuery).AsList();
            }

            Tag = tags.Select(t => new SelectListItem
            {
                Text = t.TagDescription,
                Value = t.PostTagId.ToString()
            }).ToList();

            Category = categories.Select(c => new SelectListItem
            {
                Text = c.CategoryDescription,
                Value = c.CategoryID.ToString()
            }).ToList();
        }

    }
}
