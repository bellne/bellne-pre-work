﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Models
{
    public class TagListVM
    {
        public List<Post> TaggedPostList { get; set; }
        public string tagName { get; set; }

    }
}
