﻿using System.Configuration;

namespace JKNGCapstone.Data
{
    public static class Settings
    {
        private static string _connectionString;

        public static string ConnectionString
        {
            get
            {
                if (ConfigurationManager.AppSettings["DbMode"] == "prod")
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["aspnet-JKNGCapstone.UI-20160422091724"].ConnectionString;
                } else
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["Test Connection"].ConnectionString;
                }

                return _connectionString;
            }
        }
    }
}