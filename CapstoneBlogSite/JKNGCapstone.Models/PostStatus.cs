﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKNGCapstone.Models
{
    public class PostStatus
    {
        public int PostStatusId { get; set; }
        public string StatusDescription { get; set; }
    }
}