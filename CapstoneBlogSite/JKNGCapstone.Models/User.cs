﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKNGCapstone.Models
{
    public class User
    {
        public string UserId { get; set; } //aspNet uses a string for UserId (updated to string from int)
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}