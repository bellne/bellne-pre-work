﻿using Dapper;
using JKNGCapstone.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JKNGCapstone.Models
{
    public class AddStaticPageVM
    {
        public List<Post> StaticPageList {get; set; }

        [Required(ErrorMessage = "Please choose at lease one category")]
        public Post post { get; set; }
        public int Count { get; set; }
    }
}
