USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[GetTagDescription]    Script Date: 4/23/2016 6:13:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTagDescription]
(
	@PostTagID int
) AS

Select PostTagID, TagDescription
From Tag
Where PostTagID = @PostTagID
GO


