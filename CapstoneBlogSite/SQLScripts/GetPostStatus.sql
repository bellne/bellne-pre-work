USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[GetPostStatus]    Script Date: 4/23/2016 6:13:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPostStatus]
(
	@PostStatusId int
) AS

SELECT PostStatusId, StatusDescription
FROM [Status]
WHERE PostStatusId = @PostStatusId
GO


