USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[GetUserDescriptions]    Script Date: 4/23/2016 6:13:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUserDescriptions]
(
	@UserId int
) AS

SELECT UserId, UserName
FROM Author
WHERE Author.UserId = @UserId
GO


