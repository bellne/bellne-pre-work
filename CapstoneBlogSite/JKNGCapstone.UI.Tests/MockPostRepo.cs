﻿using JKNGCapstone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Tests
{
    public class MockPostRepo : IBlogRepository
    {
        private List<Post> _testPostRepo = new List<Post>();
        private List<User> _testUserRepo = new List<User>();

        public void DeactivatePage(int id)
        {
            throw new NotImplementedException();
        }

        public void DeletePost(int id)
        {
            _testPostRepo.RemoveAll(p => p.PostId == id);
        }

        public void DeleteStaticPage(int id)
        {
            throw new NotImplementedException();
        }

        public void EditPost(Post post)
        {
            DeletePost(post.PostId);
            Insert(post);
        }

        public void EditStatic(Post post)
        {
            throw new NotImplementedException();
        }

        public List<Post> GetAllPosts()
        {
            return _testPostRepo;
        }

        public List<Post> GetAllPostsNotPublished()
        {
            throw new NotImplementedException();
        }

        public List<Post> GetAllStaticPages()
        {
            throw new NotImplementedException();
        }

        public List<User> GetAllUsers()
        {
            return _testUserRepo;
        }

        public Post GetPostByPostId(int id)
        {
            return _testPostRepo.SingleOrDefault(p => p.PostId == id);
        }

        public CatListVM GetPostsByPostCategory(string category)
        {
            throw new NotImplementedException();
        }

        public Post GetStaticPageById(int id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Post post)
        {
            _testPostRepo.Add(post);
        }

        public void InsertStaticPage(Post post)
        {
            _testPostRepo.Add(post);
        }

        public void PublishPost(int id)
        {
            throw new NotImplementedException();
        }

        public TagListVM GetPostsByPostTag(string tag)
        {
            throw new NotImplementedException();
        }
    }
}
