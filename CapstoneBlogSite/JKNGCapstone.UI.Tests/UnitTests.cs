﻿using Dapper;
using JKNGCapstone.Data;
using JKNGCapstone.Models;
using JKNGCapstone.UI.Controllers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace JKNGCapstone.Tests
{
    [TestFixture]
    public class UnitTests
    {
        IBlogRepository _mockRepo = new MockPostRepo();
        IBlogRepository _repo = new BlogRepository();
        DateTime now = DateTime.Now;

        [SetUp]
        public void SetUp()
        {

            _mockRepo.Insert(new Post { PostId = 1, PostTitle = "My Post", PostBody = "Things I wrote", DateCreated = now, IsStatic = true, UserId = 1, PostStatusId = 1, PostCategory = 1, PostTagId = 1 });
            _mockRepo.Insert(new Post { PostId = 2, PostTitle = "Another Post", PostBody = "Things and stuff", DateCreated = now, IsStatic = true, UserId = 2, PostStatusId = 1, PostCategory = 2, PostTagId = 1 });
            _mockRepo.Insert(new Post { PostId = 3, PostTitle = "More things I know", PostBody = "Things I wrote", DateCreated = now, IsStatic = true, UserId = 1, PostStatusId = 1, PostCategory = 2, PostTagId = 2 });
            _mockRepo.Insert(new Post { PostId = 4, PostTitle = "A thing I'd like to tell you", PostBody = "Things and stuff", DateCreated = now, IsStatic = true, UserId = 2, PostStatusId = 1, PostCategory = 1, PostTagId = 2 });
        }

        [TearDown]
        public void TearDown()
        {
            List<Post> postList = _mockRepo.GetAllPosts();
            postList.RemoveRange(0, postList.Count);
        }

        [Test]
        public void EnsureAddResultIsNotNull()
        {
            BlogController controller = new BlogController(_mockRepo);

            ViewResult result = controller.Add() as ViewResult;
            
            Assert.IsNotNull(result);

        }

        [Test]
        public void EnsureAddStaticPageIsNotNull()
        {
            BlogController Controller = new BlogController(_mockRepo);

            ViewResult result = Controller.AddStaticPage() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestCase(1)]
        [TestCase(50)]
        [TestCase(400)]
        public void EnsureIndexIsNotNull(int test)
        {
            BlogController Controller = new BlogController(_mockRepo);
            ViewResult result = Controller.Index(test) as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void InsertTest()
        {
            BlogController controller = new BlogController(_mockRepo);

            //set up AddPostVM
            SelectListItem tag1 = new SelectListItem { Text = "atag", Value = "1" };
            SelectListItem cat1 = new SelectListItem { Text = "acat", Value = "1" };
            List<Category> CategoryDescription = new List<Category> {
                   new Category() {CategoryDescription="Whiskey" }
            };
            List<PostTag> TagDesc = new List<PostTag> {
                   new PostTag() { TagDescription="Brunch"}
            };
            string[] testcatIds = new string[] { "1" };
            string[] testtagIds = new string[] { "2" };
            AddPostVM model = new AddPostVM
            {
                Post = new Post { PostTitle = "A Nice New Post", PostBody = "Things and stuff", TagIds = testtagIds, CategoryIds = testcatIds, PostTag = TagDesc, PostCategoryDescription = CategoryDescription, DateCreated = DateTime.Now, IsStatic = true, UserId = 2, PostStatusId = 1, PostCategory = 1, PostTagId = 2 },
                Tag = new List<SelectListItem> { tag1 },
                Category = new List<SelectListItem> { cat1 }
            };

            var result = controller.Add(model) as RedirectToRouteResult;

            result.RouteValues["Action"].Equals("AdminIndex");
            result.RouteValues["Controller"].Equals("Blog");
        }

        [Test]
        public void EditPostTest()
        {
            BlogController blogController = new BlogController(_mockRepo);
            BlogController postController = new BlogController(_mockRepo);

            //set up AddPostVM
            SelectListItem tag1 = new SelectListItem { Text = "atag", Value = "1" };
            SelectListItem cat1 = new SelectListItem { Text = "acat", Value = "1" };
            List<Category> CategoryDescription = new List<Category> {
                   new Category() {CategoryDescription="Whiskey" }
            };
            List<PostTag> TagDesc = new List<PostTag> {
                   new PostTag() { TagDescription="Brunch"}
            };
            string[] testcatIds = new string[] { "1" };
            string[] testtagIds = new string[] { "2" };
            AddPostVM model = new AddPostVM
            {
                Post = new Post { PostTitle = "A Nice New Post", PostBody = "Things and stuff", TagIds = testtagIds, CategoryIds = testcatIds, PostTag = TagDesc, PostCategoryDescription = CategoryDescription, DateCreated = DateTime.Now, IsStatic = true, UserId = 2, PostStatusId = 1, PostCategory = 1, PostTagId = 2 },
                Tag = new List<SelectListItem> { tag1 },
                Category = new List<SelectListItem> { cat1 }
            };

            postController.Add(model);

            model.Post.PostTitle = "Edited Title";

            blogController.Edit(model);

            Assert.True(model.Post.PostTitle=="Edited Title");
            }
        

        [Test]
        public void GetPostByPostIdTest()
        {
            int id = 4;
            var repo = new BlogRepository();
            Post post = repo.GetPostByPostId(id);
            var actual = post.PostTitle;
            var expected = "Post Four";

            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void PublishPostTest()
        {
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                var publishPost = sqlConnection.Query<Post>("SELECT PostID FROM Post").Last();
                int id = publishPost.PostId;

                _repo.PublishPost(id);

                var lastPost = sqlConnection.Query<Post>("SELECT PostStatusId FROM Post").Last();
                int actual = lastPost.PostStatusId;
                int expected = 2;

                Assert.AreEqual(actual, expected);
            }
        }
    }
}
