﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.Models
{
    interface IHotel
    {
        string HotelName { get; set; }
        string HotelCity { get; set; }
        string HotelState { get; set; }
    }
}
