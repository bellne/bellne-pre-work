﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class MarriottHotel : IHotel
    {
        private string hotelCity = "Akron";
        public string HotelCity
        {
            get
            {
                return hotelCity;
            }

            set
            {
                hotelCity = value;
            }
        }

        private string hotelName = "Marriott";
        public string HotelName
        {
            get
            {
                return hotelName;
            }

            set
            {
                hotelName = value;
            }
        }

        private string hotelState = "OH";
        public string HotelState
        {
            get
            {
                return hotelState;
            }

            set
            {
                hotelState = value;
            }
        }
    }
}