﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock(); // 1
            //InStockAndMoreThanThree(); // 2
            //CustomersWashingtonNameAndOrders(); // 3
            //NewSequenceProductNames(); // 4
            //NewSequenceProductPricesIncreased25(); // 5
            //ProductNamesUpperCase(); // 6
            //ProductsWithEvenUnitsInStock(); // 7
            //ProdNamePriceCatRename(); // 8
            //NumberPairs(); // 9
            //CustIdOrderIdTotal(); // 10
            //FirstThree(); // 11
            //FirstThreeCustomerOrders(); // 12
            //SkipFirstThree(); // 13
            //SkipFirstTwoWA(); // 14
            //GreaterOrEqualToSix(); // 15
            //LessThanArrayPosition(); // 16
            //DivisibleByThree(); // 17
            //AlphabeticalProducts(); // 18
            //UnitsInStockDescending(); // 19
            //SortedByCategoryAndPrice(); // 20
            //ReverseC(); // 21
            //GroupedByRemainder(); // 22
            //ProductsByCategory(); // 23
            //GroupCustOrders(); // 24
            //GroupEm(); // 24 *provided solution
            //UniqueProdCatNames(); // 25
            //UniqueValues(); // 26
            //SharedValues(); // 27
            //UniqueNumbersA(); // 28
            //FirstProdId12(); // 29
            //Prod789Exist(); // 30
            //ListCatOutOfStock(); // 31
            //NumsBLessThan9(); // 32
            //GroupedAllInStock(); // 33
            //CategoriesAllInStock(); // 33 *provided solution
            //NumberOfOdds(); // 34
            //CustIdsAndNumOrders(); // 35
            //CatListAndProdCount(); // 36
            //CatTotalUnitsInStock(); // 37
            //LowestPricedProductInCategory(); // 38
            //HighestPricedProductInCategory(); // 39
            //AverageProductPricePerCategory(); // 40

            //GroupEm();

            Console.ReadLine();
        }
        // 1
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        // 2
        private static void InStockAndMoreThanThree()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        // 3
        private static void CustomersWashingtonNameAndOrders()
        {
            var customers = DataLoader.LoadCustomers();

            var customersInWashington = customers.Where(p => p.Region == "WA");

            foreach (var customer in customersInWashington)
            {
                Console.WriteLine(customer.CompanyName);

                foreach (var order in customer.Orders)
                {
                    Console.WriteLine(order.OrderID + "," + order.OrderDate + "," + order.Total);
                }

                Console.WriteLine("\n");
            }
        }
        // 4
        private static void NewSequenceProductNames()
        {
            var allProducts = DataLoader.LoadProducts();
            var result = from product in allProducts
                         select product.ProductName;
            foreach(var i in result)
            {
                Console.WriteLine(i);
            }            
        }
        // 5
        private static void NewSequenceProductPricesIncreased25()
        {
            decimal priceIncrease = 1.25M;

            var productsList = DataLoader.LoadProducts();
            var result = from products in productsList
                         select new { products.ProductName, newPrice = products.UnitPrice * priceIncrease };

            foreach(var i in result)
            {
                Console.WriteLine(i);
            }
        }
        // 6
        private static void ProductNamesUpperCase()
        {
            var allProducts = DataLoader.LoadProducts();
            var allProductsUpperCase = from product in allProducts
                                       select product.ProductName.ToUpper();
            foreach(var i in allProductsUpperCase)
            {
                Console.WriteLine(i);
            }
        }
        // 7
        private static void ProductsWithEvenUnitsInStock()
        {
            var allProducts = DataLoader.LoadProducts();

            var evenProductsList = from product in allProducts
                                   where product.UnitsInStock % 2 == 0
                                   select new { product.ProductName, product.UnitsInStock };
            foreach (var i in evenProductsList)
            {
                Console.WriteLine(i);
            }
        }
        // 8
        private static void ProdNamePriceCatRename()
        {
            var allProducts = DataLoader.LoadProducts();
            var propertyNameChange = from all in allProducts
                                     select new { all.ProductName, all.Category, Price = all.UnitPrice };
            foreach(var i in propertyNameChange)
            {
                Console.WriteLine(i);
            }
        }
        // 9
        private static void NumberPairs()
        {
            var result = from b in DataLoader.NumbersB
                         from c in DataLoader.NumbersC
                         where b < c
                         select new { b, c }; 
            foreach(var i in result)
            {
                Console.WriteLine(i);
            }
        }
        // 10
        private static void CustIdOrderIdTotal()
        {
            var resultSet = DataLoader.LoadCustomers();

            var results = from res in resultSet
                          from order in res.Orders
                          where order.Total < 500
                          select new { res.CustomerID, order.OrderID, order.Total };

            foreach (var i in results)
            {
                Console.WriteLine(i);
            }
         }
        // 11
        private static void FirstThree()
        {
            var numbersArray = DataLoader.NumbersA;

            var firstThree = numbersArray.Take(3);
            foreach(var i in firstThree)
            {
                Console.WriteLine(i);
            }
        }
        // 12
        private static void FirstThreeCustomerOrders()
        {
            var fullCustomerInfo = DataLoader.LoadCustomers();

            var results = from prod in fullCustomerInfo
                          from ord in prod.Orders.Take(3)
                          where prod.Region == "WA"
                          select new { prod.CompanyName, ord.OrderDate, ord.OrderID, ord.Total };

            foreach (var i in results)
            {
                Console.WriteLine(i.CompanyName);
                foreach(var j in results)
                    Console.Write(i.OrderID + "\t");
                    Console.Write(i.OrderDate + "\t");
                    Console.Write(i.Total + "\n");                
            };
        }
        // 13
        private static void SkipFirstThree()
        {
            var result = DataLoader.NumbersA.Skip(3);
            foreach(var i in result)
            {
                Console.WriteLine(i);
            }

        }
        // 14
        private static void SkipFirstTwoWA()
        {
            var fullDataSet = DataLoader.LoadCustomers();
            var result = from var in fullDataSet
                          where var.Region == "WA"                        
                          from ord in var.Orders.Skip(2)
                          select new { var.CompanyName, ord.OrderID };                          

            foreach (var i in result)
            {
                Console.WriteLine("\n" + i.CompanyName);                
                Console.WriteLine(i.OrderID);             
            }
        }
        // 15
        private static void GreaterOrEqualToSix()
        {
            var data = from num in DataLoader.NumbersC.TakeWhile(x => x <= 6)
                       select num;
            foreach(var i in data)
            {
                Console.Write(i + " ");
            }
                       
        }
        // 16
        private static void LessThanArrayPosition()
        {
            var result = from num in DataLoader.NumbersC.TakeWhile(x => x >= DataLoader.NumbersC[+1])
                         select num;

            foreach (var n in result)
            {
                Console.Write(n + " ");
            }
        }
        // 17
        private static void DivisibleByThree()
        {
            var result = from num in DataLoader.NumbersC.SkipWhile(x => !(x % 3 == 0))
                         select num;
            foreach(var i in result)
            {
                Console.Write(i + " ");
            }
        }
        // 18
        private static void AlphabeticalProducts()
        {
            var allProducts = DataLoader.LoadProducts();
            var names = from val in allProducts.OrderBy(var => var.ProductName)
                        select val.ProductName;
            foreach(var i in names)
            {
                Console.WriteLine(i);
            }            
        }
        // 19
        private static void UnitsInStockDescending()
        {
            var resultSet = DataLoader.LoadProducts();
            var result = from info in resultSet.OrderByDescending(info => info.UnitsInStock)
                         select new { info.ProductName, info.UnitsInStock };

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }                           
        }
        // 20
        private static void SortedByCategoryAndPrice()
        {
            var productList = DataLoader.LoadProducts();
            var results = productList.OrderByDescending(p => p.UnitPrice).OrderBy(n => n.Category);

            foreach(var i in results)
            {
                Console.WriteLine(i.Category + "  " + i.ProductName + "  " + i.UnitPrice);                
            } 
        }
        // 21
        private static void ReverseC()
        {
            int[] data = DataLoader.NumbersC;
            Array.Reverse(data);
            foreach(var i in data)
            {
                Console.Write(i + "  ");
            }
        }
        // 22
        private static void GroupedByRemainder()
        {
            int[] data = DataLoader.NumbersC;
            int numLength = data.Length;

            for (int i = 0; i < numLength; i++)
            {
                int x = data[i];
                data[i] = x % 5;
                Console.WriteLine(data[i]);
            }
        }
        // 23
        private static void ProductsByCategory()
        {
            var products = DataLoader.LoadProducts();
            var proCat = from cat in products.OrderBy(cat => cat.Category)
                         select new { cat.Category, cat.ProductID, cat.ProductName };

            foreach(var i in proCat)
            {
                Console.WriteLine(i);
            }

        }
        // 24 My solution
        private static void GroupCustOrders()
        {
            var dataSet = DataLoader.LoadCustomers();

            var results = from ord in dataSet
                          from prod in ord.Orders.OrderBy(prod => prod.OrderDate.Month).OrderBy(prod => prod.OrderDate.Year)
                          select new { ord.CompanyName, prod.OrderDate, prod.OrderID, prod.Total };
            foreach(var i in results)
            {
                Console.WriteLine(i.OrderDate + " " + i.CompanyName + " " + i.Total);
            }

        }        
        // 24 Group customer orders by year, then by month.
        private static void GroupEm()
        {
            var customers = DataLoader.LoadCustomers();
            var result = from customer in customers
                         from order in customer.Orders
                         group order by order.OrderDate.Year into yr
                         select new
                         {
                             Year = yr.Key,
                             MonthGroups = from yrs in yr
                                           group yrs by yrs.OrderDate.Month into mnths
                                           select new { Month = mnths.Key, Orders = mnths}
                         };

            var a = "a";
            
        }
        // 25
        private static void UniqueProdCatNames()
        {
            var dataSet = DataLoader.LoadProducts();

            var prodNames = (from prod in dataSet
                            select prod.Category).Distinct();

            foreach(var i in prodNames)
            {
                Console.WriteLine(i);
            }

        }
        // 26
        private static void UniqueValues()
        {
            var result = DataLoader.NumbersA.Except(DataLoader.NumbersB);
            var result2 = DataLoader.NumbersB.Except(DataLoader.NumbersA);
            var result3 = result.Concat(result2);
            
            foreach(var i in result3)
            {
                Console.WriteLine(i);
            }
        }
        // 27
        private static void SharedValues()
        {
            var result1 = from i in DataLoader.NumbersA
                          join m in DataLoader.NumbersB on i equals m
                          select m;

            foreach(var a in result1)
            {
                Console.WriteLine(a);
            }
        }
        // 28
        private static void UniqueNumbersA()
        {
            var results = DataLoader.NumbersA.Except(DataLoader.NumbersB);

            foreach(var i in results)
            {
                Console.WriteLine(i);
            }
        }
        // 29 
        private static void FirstProdId12()
        {
            var fullData = DataLoader.LoadProducts();
            var result = fullData.First(p => p.ProductID == 12);
            Console.WriteLine(result.ProductID + " " + result.ProductName);
        }
        // 30
        private static void Prod789Exist()
        {
            var fullData = DataLoader.LoadProducts();
            var result = fullData.FirstOrDefault(p => p.ProductID == 789);
            if(result == null)
            {
                Console.WriteLine("ProductID 789 doesn't exist");
            }
            else
            {
                Console.WriteLine(result.ProductID + " " + result.ProductName);
            }
        }
        // 31
        private static void ListCatOutOfStock()
        {
            var dataSet = DataLoader.LoadProducts();
            var result = from i in dataSet
                         where i.UnitsInStock == 0
                         select (i.Category);
            var disctinctCats = result.Distinct();

            foreach(var i in disctinctCats)
            {
                Console.WriteLine(i);
            }
        }
        // 32
        private static void NumsBLessThan9()
        {
            bool result = DataLoader.NumbersB.All(x => x < 9);
            Console.WriteLine(result);
        }
        // 33
        private static void GroupedAllInStock()
        {
            var dataSet = DataLoader.LoadProducts();
            var categories = dataSet.GroupBy(p => p.Category);

            foreach (var group in categories)
            {
                if (group.All(p => p.UnitsInStock > 0))
                {
                    Console.WriteLine(group.Key);
                    Console.WriteLine("---------------");
                    foreach (var product in group)
                    {
                        Console.WriteLine("{0, 35} {1}", product.ProductName, product.UnitsInStock);
                    }

                    Console.WriteLine();
                }
            }
        }
        // 33 Provided Solution
        private static void CategoriesAllInStock()
        {
            var products = DataLoader.LoadProducts();

            var categories = products.GroupBy(p => p.Category);

            foreach (var group in categories)
            {
                if (group.All(p => p.UnitsInStock > 0))
                {
                    Console.WriteLine(group.Key);
                    Console.WriteLine("-----------------");
                    foreach (var product in group)
                    {
                        Console.WriteLine("{0, 35} {1}", product.ProductName, product.UnitsInStock);
                    }

                    Console.WriteLine();
                }
            }
        }
        // 34
        private static void NumberOfOdds()
        {
            var results = DataLoader.NumbersA.Count(x => x % 2 == 1);
            Console.WriteLine(results);
        }
        // 35
        private static void CustIdsAndNumOrders()
        {
            var customers = DataLoader.LoadCustomers();

            // method syntax
            var custIds = customers.Select(x => new
            {
                CustomerId = x.CustomerID,
                NumberofOrders = x.Orders.Count()
            });
            foreach(var i in custIds)
            {
                Console.WriteLine(i);
            }

            // query syntax
            var custIdsTwo = from customer in customers
                             select new {CustomerId = customer.CustomerID, NumberOfOrders = customer.Orders.Count()
                             };
        }
        // 36 
        private static void CatListAndProdCount()
        {
            var products = DataLoader.LoadProducts();
            var categories = products.GroupBy(p => p.Category).Select(x=> new
            {
                Category = x.Key,
                TotalUnits = x.Sum(y => y.UnitsInStock)
            });
            foreach (var a in categories)
            {
                Console.WriteLine(a.Category);
                Console.WriteLine(a.TotalUnits);
            }
        }
        // 37
        private static void CatTotalUnitsInStock()
        {
            var products = DataLoader.LoadProducts();
            var categories = products.GroupBy(x => x.Category);

            var result = from z in categories
                         select new { z.Key, TotalUnitsInStock = z.Sum(p => p.UnitsInStock) };

            foreach (var a in result)
            {
                Console.WriteLine(a.Key);
                Console.WriteLine(a.TotalUnitsInStock);
            }
        }
        // 38
        private static void LowestPricedProductInCategory()
        {
            var products = DataLoader.LoadProducts();
            var categories = products.GroupBy(x => x.Category).Select(y=>y.OrderBy(z=>z.UnitPrice))
                .Select(y=>y.First());
            //var results = from y in categories
            //              select new { y.Key, LowestPrice = y(z => z.UnitPrice < z.UnitPrice) };
            foreach (var a in categories)
            {
                Console.WriteLine(a.Category);
                Console.WriteLine(a.UnitPrice);
            }
        }
        // 39
        private static void HighestPricedProductInCategory()
        {
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(x => x.Category).Select(y => y.OrderByDescending(z => z.UnitPrice))
                .Select(a => a.First());
            foreach(var i in results)
            {
                Console.WriteLine(i.Category);
                Console.WriteLine(i.UnitPrice);
            }
        }
        // 40
        private static void AverageProductPricePerCategory()
        {
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(x => x.Category).Select(y => new { Category = y.Key,
                AveragePrice = y.Average(z => z.UnitPrice) });
            foreach (var i in results)
            {
                Console.WriteLine(i.Category);
                Console.WriteLine(i.AveragePrice);
            }
        }
    }
}
