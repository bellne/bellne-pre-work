USE Northwind;
GO

SELECT *
FROM Products

SELECT *
FROM Products
WHERE ProductName = 'Queso Cabrales'

SELECT ProductName, UnitsInStock
FROM Products
WHERE ProductName IN ('Laughing Lumberjack Lager', 'Outback Lager', 'Ravioli Angelo')

SELECT *
FROM Products
WHERE UnitPrice > 20.00

SELECT *
FROM Products
WHERE UnitPrice <= 20.00

SELECT *
FROM Products
WHERE UnitPrice BETWEEN 10.00 AND 13.00