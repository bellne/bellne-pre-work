﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupsConditionals;

namespace WarmupsConditionals.Test
{
    [TestFixture]
    public class TTTTest
    {
        [Test]
        public void AreWeInTrouble()
        {
            // Arrange
            ConditionalsWarmups warmupObject1 = new ConditionalsWarmups();
            bool aSmile = true;
            bool bSmile = true;
            bool expected = true;

            // Act
            bool actual = warmupObject1.AreWeInTrouble(aSmile, bSmile);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void CanSleepInTest()
        {
            // Arrange
            ConditionalsWarmups warmupObject2 = new ConditionalsWarmups();
            bool isWeekday = false;
            bool isVacation = false;
            bool expected = true;

            // Act
            bool actual = warmupObject2.CanSleepIn(isWeekday, isVacation);

            // Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void SumDoubleTest()
        {
            // Arrange
            ConditionalsWarmups warmupObject3 = new ConditionalsWarmups();
            int a = 2;
            int b = 2;
            int expected = 8;

            // Act
            int actual = warmupObject3.SumDouble(a, b);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Diff21Test()
        {
            // Arrange 
            ConditionalsWarmups warmupObject4 = new ConditionalsWarmups();
            int n = 23;
            int expected = 4;

            // Act
            int actual = warmupObject4.Diff21(n);

            // Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void ParrotTroubleTest()
        {
            // Arrange
            ConditionalsWarmups warmupObject5 = new ConditionalsWarmups();
            bool isTalking = false;
            int hour = 6;
            bool expected = false;

            // Act
            bool actual = warmupObject5.ParrotTrouble(isTalking, hour);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Makes10Test()
        {
            ConditionalsWarmups warmupObject6 = new ConditionalsWarmups();
            int testA = 9;
            int testB = 9;
            bool expected = false;
            bool actual = warmupObject6.Makes10(testA, testB);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void NearHundredTest()
        {
            ConditionalsWarmups warmupObject7 = new ConditionalsWarmups();
            int test = -103;
            bool expected = true;
            bool actual = warmupObject7.NearHundred(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PosNegTest()
        {
            ConditionalsWarmups warmupObject8 = new ConditionalsWarmups();
            int testA = -1;
            int testB =-1;
            bool testBool = true;
            bool expected = true;
            bool actual = warmupObject8.PosNeg(testA, testB, testBool);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void NotStringTest()
        {
            ConditionalsWarmups warmupObject9 = new ConditionalsWarmups();
            string test = "x";
            string expected = "not x";
            string actual = warmupObject9.NotString(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void MissingCharTest()
        {
            ConditionalsWarmups warmupObject10 = new ConditionalsWarmups();
            string str = "kitten";
            int n = 4;
            string expected = "kittn";
            string actual = warmupObject10.MissingChar(str, n);
            Assert.AreEqual(expected, actual);
        }
    }
}

