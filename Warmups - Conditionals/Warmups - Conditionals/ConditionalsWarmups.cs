﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupsConditionals
{
    public class ConditionalsWarmups
    {
        // 1
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            bool result = false;
            if (aSmile && bSmile == true || aSmile && bSmile == false)
            {
                return result = true;
            }
            else return result;
        }
        // 2
        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            bool result = false;

            if (isWeekday == false || isVacation == true)
            {
                return result = true;
            }
            else
                return result = false;
        }
        // 3
        public int SumDouble(int a, int b)
        {
            int sum = 0;

            if (a == b)
            {
                return sum = 2 * (a + b);
            }
            else
            {
                return sum = a + b;
            }
        }
        // 4
        public int Diff21(int n)
        {
            int result = 0;

            if (n > 21)
            {
                return result = 2 * Math.Abs(21 - n);
            }
            else
            {
                return result = Math.Abs(21 - n);
            }
        }
        // 5
        public bool ParrotTrouble(bool isTalking, int hour)
        {
            bool result = true;

            if ((hour < 7 && isTalking == true) || (hour > 20 && isTalking == true))
            {
                return result = true;
            }

            return result = false;
        }
        // 6
        public bool Makes10(int a, int b)
        {
            bool result = true;

            if(a == 10)
            {
                result = true;
                return result;
            }
            else if(b == 10)
            {
                result = true;
                return result;
            }
            else if(a + b == 10)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return false;
            }
        }
        // 7
        public bool NearHundred(int n)
        {
            int absN = Math.Abs(n);
            bool result = false;
            
            if(90 <= absN && absN <= 110)
            {
                result = true;
                return result;
            }
            else if(190 <= absN && absN <= 210)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return result;
            }
        }
        // 8
        public bool PosNeg(int a, int b, bool negative)
        {
            bool result = false;

            if ((a > 0 && b < 0 && negative == false) || (a < 0 && b > 0 && negative == false))
            {
                result = true;
                return result;
            }
            else if (a < 0 && b < 0 && negative == true)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return result;
            }
        }
        // 9
        public string NotString(string s)
        {
            string result = "";
            string not = "not ";
            int sLength = s.Length;


            if(sLength < 5)
            {
                result = not + s;
                return result;
            }
            else if(s.Substring(0,4) == not)
            {
                result = s;
                return result;
            }
            else
            {
                result = not + s;
                return result;
            }
        }
        // 10
        public string MissingChar(string str, int n)
        {
            string result = str.Remove(n, 1);
            return result;
        }

    }
}

