﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class WarmupsLogic
    {
        // 1
        public bool GreatParty(int cigars, bool isWeekend)
        {
            bool result;
            if (isWeekend == true)
            {
                if (cigars >= 40)
                {
                    return result = true;
                }
                else
                    return result = false;
            }
            else 
            {
                if (cigars >= 40 && cigars <= 60)
                {
                    return result = true;
                }
                else
                    return result = false;
            
            }
        }
        // 2
        public int CanHazTable(int yourStyle, int dateStyle)
        {
            int no = 0;
            int maybe = 1;
            int yes = 2;
            if (yourStyle <= 2 || dateStyle <= 2)
            {
                return no;
            }
            else if (yourStyle >= 8 || dateStyle >= 8)
            {
                return yes;
            }
            else
                return maybe;
        }
        // 3
        public bool PlayOutside(int temp, bool isSummer)
        {
            bool result = false;
            if (isSummer == true && temp >= 60 && temp <= 100)
            {
                return result = true;
            }
            else if (isSummer == false && temp >= 60 && temp <= 90)
            {
                return result = true;
            }
            else return result = false;
        }
        // 4
        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            int noTicket = 0;
            int smallTicket = 1;
            int bigTicket = 2;
            if (isBirthday == false)
            {
                if (speed < 61)
                {
                    return noTicket;
                }
                else if (speed > 60 && speed < 81)
                {
                    return smallTicket;
                }
                else
                    return bigTicket;
            }
            else if (isBirthday == true)
            {
                if (speed < 66)
                {
                    return noTicket;
                }
                else if (speed > 65 && speed < 86)
                {
                    return smallTicket;
                }
                else
                    return bigTicket;
            }
            else
                return 0;
        }
        // 5
        public int SkipSum(int a, int b)
        {
            int sum = a + b;
            int result = 0;
            if (sum >= 10 && sum <= 19)
            {
                return result = 20;
            }
            else
                return sum;
        }
        // 6
        public string AlarmClock(int day, bool vacation)
        {
            string result = "";

            if ((day == 1 || day == 2 || day == 3 || day == 4 || day == 5) && vacation == false)
            {
                result = "7:00";
                return result;
            }
            else if ((day == 0 || day == 6) && vacation == false)
            {
                result = "10:00";
                return result;
            }
            else if ((day == 1 || day == 2 || day == 3 || day == 4 || day == 5) && vacation == true)
            {
                result = "10:00";
                return result;
            }
            else
            {
                result = "off";
                return result;
            }
        }
        // 7
        public bool LoveSix(int a, int b)
        {
            bool result = true;

            if (a == 6 || b == 6 || a + b == 6 || a - b == 6 || b - a == 6)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return result;
            }
        }
        // 8
        public bool InRange(int n, bool outsideMode)
        {
            bool result = true;
            
            if((n >= 1 && n <= 10 && outsideMode == false) || ((n <= 1 || n >= 10) && outsideMode == true))
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return false;
            }
        }
        // 9
        public bool SpecialEleven(int n)
        {
            bool result = true;

            if(n % 11 == 0 || (n - 1) % 11 == 0)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return false;
            }            
        }
        // 10
        public bool Mod20(int n)
        {
            bool result = true;

            if((n - 1) % 20 == 0 || (n - 2) % 20 == 0)
            {
                result = true;
                return result;
            }
            else
            {
                result = false;
                return false;
            }
        }

    }
}