﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Warmups;

namespace WarmupsLogicTest.Test
{
    [TestFixture]
    public class TTTTest
    {
        [Test]
        public void GreatPartyTest()
        {
            // Arrange
            WarmupsLogic warmupObject1 = new WarmupsLogic();
            int cigars = 70;
            bool weekend = true;
            bool expected = true;

            // Act
            bool actual = warmupObject1.GreatParty(cigars, weekend);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void CanHazTableTest()
        {
            WarmupsLogic warmupObject2 = new WarmupsLogic();
            int you = 5;
            int date = 5;
            int expected = 1;

            int actual = warmupObject2.CanHazTable(you, date);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PlayOutsideTest()
        {
            WarmupsLogic newObject3 = new WarmupsLogic();
            int temp = 95;
            bool isSummer = true;
            bool expected = true;

            bool actual = newObject3.PlayOutside(temp, isSummer);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void CaughtSpeedingTest()
        {
            WarmupsLogic warmupObject4 = new WarmupsLogic();
            int speed = 65;
            bool isBirthday = true;
            int expected = 0;

            int actual = warmupObject4.CaughtSpeeding(speed, isBirthday);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void SkipSumTest()
        {
            WarmupsLogic warmupObject5 = new WarmupsLogic();
            int a = 10;
            int b = 11;
            int expected = 21;
            int actual = warmupObject5.SkipSum(a, b);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void AlarmClockTest()
        {
            WarmupsLogic warmupObject6 = new WarmupsLogic();
            int day = 0;
            bool vaca = false;
            string expected = "10:00";
            string actual = warmupObject6.AlarmClock(day, vaca);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void LoveSixTest()
        {
            WarmupsLogic warmupObject7 = new WarmupsLogic();
            int a = 4;
            int b = 5;
            bool expected = false;
            bool actual = warmupObject7.LoveSix(a, b);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void InRangeTest()
        {
            WarmupsLogic warmupObject8 = new WarmupsLogic();
            int n = 11;
            bool outsideMode = true;
            bool expected = true;
            bool actual = warmupObject8.InRange(n, outsideMode);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void SpecialElevenTest()
        {
            WarmupsLogic warmupObject9 = new WarmupsLogic();
            int n = 23;
            bool expected = true;
            bool actual = warmupObject9.SpecialEleven(n);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Mod20Test()
        {
            WarmupsLogic warmupObject10 = new WarmupsLogic();
            int n = 20;
            bool expected = false;
            bool actual = warmupObject10.Mod20(n);
            Assert.AreEqual(expected, actual);
        }
    }
}
