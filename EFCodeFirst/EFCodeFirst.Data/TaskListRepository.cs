﻿using EFCodeFirst.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirst.Data
{
    public class TaskListRepository
    {
        public List<TaskList> GetAllTaskLists()
        {
            using (var db = new TaskListContext())
            {
                return db.TaskLists.ToList();
            }
        }
    }
}
