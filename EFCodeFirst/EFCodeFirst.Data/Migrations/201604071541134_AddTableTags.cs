namespace EFCodeFirst.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableTags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.TagTaskItems",
                c => new
                    {
                        Tag_TagId = c.Int(nullable: false),
                        TaskItem_TaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagId, t.TaskItem_TaskId })
                .ForeignKey("dbo.Tags", t => t.Tag_TagId, cascadeDelete: true)
                .ForeignKey("dbo.TaskItems", t => t.TaskItem_TaskId, cascadeDelete: true)
                .Index(t => t.Tag_TagId)
                .Index(t => t.TaskItem_TaskId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagTaskItems", "TaskItem_TaskId", "dbo.TaskItems");
            DropForeignKey("dbo.TagTaskItems", "Tag_TagId", "dbo.Tags");
            DropIndex("dbo.TagTaskItems", new[] { "TaskItem_TaskId" });
            DropIndex("dbo.TagTaskItems", new[] { "Tag_TagId" });
            DropTable("dbo.TagTaskItems");
            DropTable("dbo.Tags");
        }
    }
}
