window.onload = init;
function init()
{
	document.getElementById("betButton").onclick = handlebetButton;
}
function handlebetButton()
{
	var startingBet = document.getElementById("startingBet").value;
	luckySevens(startingBet);
	return false;
}
function rollSim()
				{
					var d1 = Math.floor(Math.random() * 6) + 1;
					var d2 = Math.floor(Math.random() * 6) + 1;
					var diceTotal = d1 + d2;
					return diceTotal;
				}
function luckySevens(x)
				{
					var startingMoney;
					var moneyLeft;
					var mostMoney;
					var numRolls;
					var mostRolls;
					var diceResult;
					var again;
					startingMoney = parseFloat(x);
					moneyLeft = startingMoney;
					mostMoney = moneyLeft;
					numRolls = 0;
					mostRolls = 0;
					diceResult = 0;
					
					for (moneyLeft; moneyLeft > 0;)
					{						
								diceResult = rollSim();
									if (diceResult == 7)
									{
										moneyLeft += 4;
										numRolls += 1;	
											if (moneyLeft > mostMoney)
											{
												mostMoney = moneyLeft;
												mostRolls = numRolls;
											}
									}
									else
									{
										moneyLeft -= 1;
										numRolls += 1;
									}							
					}
									
				
						initialBet("$" + startingMoney);
						totalRolls(numRolls);
						mostWon("$" + mostMoney);
						rollCount(mostRolls);	
						playAgain();
					
					/*
						again = confirm ("Play Again?");
							if (again == true)
							{
								init();
							}
							else
							{
								alert("Thanks for Playing!");
							}
					*/
						
				}
function initialBet(msg)
{
		document.getElementById("01").innerHTML = msg;
		//messageArea.innerHTML = msg;
}
function totalRolls(msg)
{
		var messageArea = document.getElementById("11");
		messageArea.innerHTML = msg;
}
function mostWon(msg)
{
		var messageArea = document.getElementById("21");
		messageArea.innerHTML = msg;
}
function rollCount(msg)
{
		var messageArea = document.getElementById("31");
		messageArea.innerHTML = msg;
}
function playAgain()
{
	var askPlayAgain = document.getElementById("betButton");
	askPlayAgain.innerHTML = "Play Again";
	if (document.getElementById("betButton").onclick)
	{
		
		init();
	}
	else
		return false;	
}

/*function handleKeyPress(e)
{
	var betButton = document.getElementById("betButton");
	if (e.keyCode === 13)
	{
		betButton.onclick();
		return false;	
	}
}
*/