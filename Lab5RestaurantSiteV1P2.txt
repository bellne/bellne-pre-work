<!DOCTYPE html>

<html>
	<head>
		<title>RestaurantSiteV1</title>
	</head>
	<body>
		<h4 id="menu">Menu</h4>
		<h1><center>Bellas</center></h1>
		<hr>

		<a href="#home"><b>Home</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#menu"><b>Menu</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#contactUs"><b>Contact Us</b></a>

		<hr>
		&nbsp&nbsp&nbsp
		<table width="100%">
			<tr>
				<td><center><b>Burger and Fries</b>
					<br>
					<font size="2">An American Classic!
					<br>
					Your choice - with or without cheese
					<br>
					Pickle, onion, tomato, and lettuce
					<br>
					Served with our famous thick cut
					<br>
					french fries
					<br>
					$7.95
					</font></center></td>
				<td><center><b>Hot Dog and Chips</b>
					<br><br>
					<font size="2">A Summer Classic!
					<br>
					Your choice - white or wheat bun
					<br>
					Relish, mustard, and ketchup
					<br>
					Served with our house-made chips
					<br>
					$7.95
					</font></center></td>
				<td><center><b>Italian Sausage and Peppers</b>
					<br><br>
					<font size="2">An All-Time Favorite!!
					<br>
					Your choice - mild or spicy
					<br>
					Cole Slaw
					<br>
					Served with garlic mashers
					<br>
					$10.95
					</font></center></td>
			</tr>

			<tr>
				<td><br><br><center><b>Tenderloin Sandwich and Chips</b>
					<br><br>
					<font size="2">Really Hits the Spot!
					<br>
					Plain or Sesame Seed Bun
					<br>
					Pickle, onion, tomato, and lettuce
					<br>
					Served with our house-made chips
					<br>
					$8.95
					</font></center></td>
				<td><br><br><br><center><b>House Salad</b>
					<br><br>
					<font size="2">On the Lighter Side
					<br>
					Your choice - with or without cheese
					<br>
					Iceberg lettuce, tomatoes, peppers
					<br>
					Thousand Island or Ranch Dressing
					<br>
					Served with a dinner roll
					<br>
					$6.95
					</font></center></td>
				<td><br><br><br><center><b>Brisket Sandwich and Fries</b>
					<br>
					<font size="2">A Taste of Texas!
					<br>
					Smoked Brisket on thick-cut Texas
					<br>
					Toast
					<br>
					Cole Slaw
					<br>
					Served with our famous thick-cut
					<br>
					french fries
					<br>
					$7.95
					</font></center></td>
			</tr>
		</table>
		<br><br>
		<hr>
		<p><center><font size="2">234 Whispering Pines Lane, Chippewa Falls, WI, 54729
		<br>
		(555) 555-5555</font></center>
		</p>
		<hr>

		<br><br><br><br><br><br><br><br><br><br><br><br>
		<h4 id="contactUs">Contact Us</h4>
		<h1><center>Bellas</center></h1>
		<hr>

		<a href="#home"><b>Home</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#menu"><b>Menu</b></a>
		&nbsp&nbsp&nbsp&nbsp
		<a href="#contactUs"><b>Contact Us</b></a>

		<hr>
		<br><br>

		<form method="POST">
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Name: <input type="text" name="myName">
				<br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Email: <input type="text" name="myEmail">
				<br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Phone: <input type="text" name="myPhone">
				<br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Reason for Inquiry:
				<select name ="reason">
					<option value="catering">Catering</option>
					<option value="privateParty">Private Party</option>
					<option value="feedback">Feedback</option>
					<option value="other">Other</option>
				</select>
					<br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Additional Information:
				<textarea name="additionalInfo" style="width:152px">
				</textarea><br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Have you been to the restaurant?
				<input type="radio" name="answer" value="No" checked>No &nbsp&nbsp&nbsp
				<input type="radio" name="answer" value="Yes">Yes<br><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Best days to contact you:
				<input type="checkbox" name="bestDays" value="MONDAY">M &nbsp
				<input type="checkbox" name="bestDays" value="TUESDAY">T &nbsp
				<input type="checkbox" name="bestDays" value="WEDNESDAY">W &nbsp
				<input type="checkbox" name="bestDays" value="THURSDAY">Th &nbsp
				<input type="checkbox" name="bestDays" value="FRIDAY">F <br><br>
			<center><input type="submit" value="Send Request"></center><br>

		<br><br><br><br><br><br><br><br>
		<hr>
		<p><center><font size="2">234 Whispering Pines Lane, Chippewa Falls, WI, 54729
		<br>
		(555) 555-5555</font></center>
		</p>
		<hr>

		</form>

	</body>
</html>