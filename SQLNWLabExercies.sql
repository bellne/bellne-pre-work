USE Northwind
GO

SELECT ProductName, CategoryName, CompanyName
FROM Products
	INNER JOIN Categories
	ON Products.CategoryID = Categories.CategoryID
	INNER JOIN	Suppliers
	ON Products.SupplierID = Suppliers.SupplierID

SELECT LastName, FirstName, TerritoryDescription
FROM Employees
	INNER JOIN EmployeeTerritories
	ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
	INNER JOIN Territories
	ON EmployeeTerritories.TerritoryID = Territories.TerritoryID

SELECT CompanyName, ContactName, OrderDate, ProductName, Country
FROM Orders
	INNER JOIN Customers
	ON Orders.CustomerID = Customers.CustomerID
	INNER JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
	INNER JOIN Products
	ON [Order Details].ProductID = Products.ProductID
	WHERE Customers.Country = 'USA' and CompanyName = 'Lonesome Pine Restaurant'

CREATE PROCEDURE UdateContactName
(
	@OldContactName nvarchar(30),
	@NewContactName nvarchar(30)
)as

update Customers
	set ContactName = @NewContactName
	where ContactName = @OldContactName
GO




SELECT Orders.*, Products.ProductName
FROM Orders
	INNER JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
	INNER JOIN Products
	ON [Order Details].ProductID = Products.ProductID
	WHERE Products.ProductName = 'Chai'

SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
	INNER JOIN Employees
		ON Orders.EmployeeID = Employees.EmployeeID

SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
	LEFT JOIN Employees
		ON Orders.EmployeeID = Employees.EmployeeID

SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
	LEFT JOIN Employees
		ON Orders.EmployeeID = Employees.EmployeeID
WHERE Orders.EmployeeID IS NULL OR LastName Like 'S%'