﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.Choices
{
    public interface IChoice
    {
        int PickRockPaperOrScissors(DayOfWeek forTest);
    }
}
