﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.Choices
{
    public class RandomChoice : IChoice
    {
        Random random = new Random();
        DateTime dt = DateTime.Now;

        public int PickRockPaperOrScissors(DayOfWeek forTest)
        {
            if (forTest == DayOfWeek.Tuesday)
            {
                return 2;
            }
            else
            {
                int choice = random.Next(1, 4);
                return choice;
            }               
        }
    }
}
