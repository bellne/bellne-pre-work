﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.KeepPlaying
{
    public class Default : IKeepPlayingBase
    {
        public int PlayAgainOrNot()
        {
            return 1;
        }
    }
}
