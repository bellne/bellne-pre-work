﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.Choices;
using RockPaperScissors.KeepPlaying;

namespace RockPaperScissors
{
    /*
     * This is the game manager, it will track the computer and player choices
     * 
     * For the purposes of this game 1 is Rock, 2 is Paper, 3 is Scissors
     * 
     * It keeps track of wins, losses, and draws
     * 
     * A working solution is in the Solved folder, try not to look at it...
     * 
     * Finish this code!
     */

    public class GameManager
    {

        private bool _keepPlaying;
        private int _computerChoice;
        private int _playerChoice;
        private string _computerChoiceText;
        private string _playerChoiceText;
        private int _wins = 0;
        private int _losses = 0;
        private int _draws = 0;


        private Random rng = new Random();

        public void Play()
        {
            _keepPlaying = false;
            RandomChoice rc = new RandomChoice();
            Default normal = new Default();

            do
            {
                Console.Clear();
                DisplayStats();
                string _ComputerchoicetText = GetComputerChoice(rc);
                string playerChoice = GetPlayerInput();
                string _playerChoiceText = GetPlayerChoice(playerChoice);
                DetermineResult(_computerChoiceText, _playerChoiceText);
                AskToKeepPlaying(normal);

            } while (_keepPlaying);
        }

        private void DisplayStats()
        {
            Console.WriteLine("Wins: {0}", _wins);
            Console.WriteLine("Losses: {0}", _losses);
            Console.WriteLine("Draws: {0}", _draws);
            Console.WriteLine();
        }

        public string GetComputerChoice(IChoice chooser)
        {
            DayOfWeek whatever = new DayOfWeek();
            _computerChoice = chooser.PickRockPaperOrScissors(whatever);

            switch (_computerChoice)
            {
                case 1:
                    _computerChoiceText = "Rock";
                    return "Rock";
                case 2:
                    _computerChoiceText = "Paper";
                    return "Paper";
                case 3:
                    _computerChoiceText = "Scissors";
                    return "Scissors";
                default:
                    return "Error";
            }
        }

        private string GetPlayerInput()
        {
            string choice = "";
            bool validInput = false;
            do
            {
                Console.WriteLine("Enter your choice!");
                Console.WriteLine("------------------");
                Console.Write("R for Rock, P for Paper, S for Scissors: ");
                choice = Console.ReadLine();
                if(choice == "R" || choice == "P" || choice == "S")
                {
                    validInput = true;
                    return choice;                    
                }
                else
                {
                    Console.WriteLine("Not a valid choice. Try again.");
                    validInput = false;
                }
            }
            while (validInput == false);
            return choice;

        }

        public string GetPlayerChoice(string choice)
        {
                       
                switch (choice)
                {
                    case "R":
                        _playerChoiceText = "Rock";
                        _playerChoice = 1;
                        return _playerChoiceText;
                    case "P":
                        _playerChoiceText = "Paper";
                        _playerChoice = 2;
                        return _playerChoiceText;
                    case "S":
                        _playerChoiceText = "Scissors";
                        _playerChoice = 3;
                        return _playerChoiceText;
                    default:
                        return "Invalid";
                }            
        }

        public string DetermineResult(string _computerChoiceText, string _playerChoiceText)
        {
            string draw = "It's a draw!";
            string win = "You won!";
            string loss = "Computer won :(";
            Console.WriteLine();
            Console.WriteLine("You picked {0}, computer picked {1}", _playerChoiceText, _computerChoiceText);

            if (_playerChoiceText == _computerChoiceText)
            {
                Console.WriteLine(draw);
                _draws++;
                return draw;
            }
            else if((_playerChoiceText == "Rock" && _computerChoiceText == "Scissors") 
                    || (_playerChoiceText == "Paper" && _computerChoiceText == "Rock")
                    || (_playerChoiceText == "Scissors" && _computerChoiceText == "Paper"))
            {
                Console.WriteLine(win);
                _wins++;
                return win;
            }
            else
            {
                Console.WriteLine(loss);
                _losses++;
                return loss;
            }
        }

        public bool AskToKeepPlaying(IKeepPlayingBase normal)
        {            
                Console.WriteLine();
                Console.Write("Play again (Y/N)? ");
                string response = Console.ReadLine();

                if (response == "Y")
                {
                    _keepPlaying = true;
                    return _keepPlaying;
                }
                else
                {
                    _keepPlaying = false;
                    return _keepPlaying;
                }            
            }
           
        }
    }

