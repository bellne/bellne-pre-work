﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RockPaperScissors;
using RockPaperScissors.Choices;
using RockPaperScissors.KeepPlaying;

namespace RockPaperScissors.Test
{
    [TestFixture]
    class TTTTest
    {
        [Test]
        public void GetComputerChoiceTest()
        {
            GameManager testObject1 = new GameManager();
            RockChoice rc = new RockChoice();
            string expected = "Rock";
            string actual = testObject1.GetComputerChoice(rc);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ComputerRockHumanWins()
        {
            GameManager testObject3 = new GameManager();
            string computerChoice = "Rock";
            string humanChoice = "Paper";
            string expected = "You won!";
            string actual = testObject3.DetermineResult(computerChoice, humanChoice);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ComputerRockHumanLoses()
        {
            GameManager testObject4 = new GameManager();
            string computerChoice = "Rock";
            string humanChoice = "Scissors";
            string actual = testObject4.DetermineResult(computerChoice, humanChoice);
            string expected = "Computer won :(";
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PlayerPlaysPaper()
        {
            GameManager testObject12 = new GameManager();
            string test = "P";
            string expected = "Paper";
            string actual = testObject12.GetPlayerChoice(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PlayerPaperHumanLoses()
        {
            GameManager testObject5 = new GameManager();
            string humanChoice = "Paper";
            string computerChoice = "Scissors";
            string expected = "Computer won :(";
            string actual = testObject5.DetermineResult(computerChoice, humanChoice);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ComputerPlaysWithDynamite()
        {
            GameManager testObject6 = new GameManager();
            DynamiteChoice dc = new DynamiteChoice();
            string expected = "Error";
            string actual = testObject6.GetComputerChoice(dc);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ComputerPlaysPaperOnTuesdays()
        {
            GameManager testObject7 = new GameManager();
            RandomChoice tues = new RandomChoice();
            DayOfWeek testData = DayOfWeek.Tuesday;
            int expected = 2;
            int actual = tues.PickRockPaperOrScissors(testData);
            Assert.AreEqual(expected, actual);
        }
        //[Test]
        //public void AskToKeepPlayingTest()
        //{
        //    GameManager testObject2 = new GameManager();
        //    Testing test = new Testing();
        //    bool expected = true;
        //    bool actual = testObject2.AskToKeepPlaying(test);
        //    Assert.AreEqual(expected, actual);
        //}
    }
}
