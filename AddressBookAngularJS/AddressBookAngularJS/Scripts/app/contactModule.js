﻿/// <reference path="../angular.js" />
var app = angular.module('contacts', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'ListCtrl',
            templateUrl: '/Routes/list.html'
        })
        .when('/edit/:contactId', {
            controller: 'EditCtrl',
            templateUrl: '/Routes/detail.html'
        })
        .when('/new', {
            controller: 'CreateCtrl',
            templateUrl: '/Routes/detail.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.factory('contactFactory', function($http) {
    var factory = {};
    var url = '/api/contacts/';

    factory.getContacts = function() {
        return $http.get(url);
    };

    factory.getContactById = function(id) {
        return $http.get(url + id);
    };

    factory.updateContact = function(id, contact) {
        return $http.put(url + id, contact);  // angular.toJson(contact)
    };

    factory.deleteContact = function(id) {
        return $http.delete(url + id);
    };

    factory.createContact = function(contact) {
        return $http.post(url, contact);
    };

    return factory;
});

app.controller('ListCtrl', function($scope, contactFactory) {
    contactFactory.getContacts()
        .success(function(data) {
            $scope.contacts = data;
        })
        .error(function(message, status) {
            alert(message + ' status: ' + status);
    });
});

app.controller('EditCtrl', function ($scope, $location, $routeParams, contactFactory) {
    contactFactory.getContactById($routeParams.contactId)
        .success(function (data) {
            $scope.contact = data;
                })
        .error(function (message, status) {
            alert(message + ' status: ' + status);
        });

    $scope.showDelete = true;

    $scope.delete = function() {
        contactFactory.deleteContact($routeParams.contactId)
            .success(function () {
                        $location.path('/');
                    })
            .error(function (message, status) {
                alert(message + ' status: ' + status);
            });
    };

    $scope.save = function() {
        contactFactory.updateContact($routeParams.contactId, $scope.contact)
            .success(function () {
                $location.path('/');
            })
            .error(function (message, status) {
                alert(message + ' status: ' + status);
            });
    };
});

app.controller('CreateCtrl', function ($scope, $location, contactFactory) {
    $scope.save = function () {
        contactFactory.createContact($scope.contact)
            .success(function () {
                $location.path('/');
            })
            .error(function (message, status) {
                alert(message + ' status: ' + status);
            });
    };

    $scope.showDelete = false;
});