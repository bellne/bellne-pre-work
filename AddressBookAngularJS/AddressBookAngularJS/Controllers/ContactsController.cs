﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AddressBookAngularJS.Data;
using AddressBookAngularJS.DTOs;
using AddressBookAngularJS.DTOs.Interfaces;

namespace AddressBookAngularJS.Controllers
{
    public class ContactsController : ApiController
    {
        private IContactRepository _repository = new InMemoryContactRepository();

        public List<Contact> Get()
        {
            return _repository.GetAll();
        }

        public Contact Get(int id)
        {
            return _repository.GetById(id);
        }

        public HttpResponseMessage Post(Contact contact)
        {
            _repository.Add(contact);

            var response = Request.CreateResponse(HttpStatusCode.Created, contact);

            string url = Url.Link("DefaultApi", new {id = contact.Id});
            response.Headers.Location = new Uri(url);

            return response;
        }

        public void Put(int id, Contact contact)
        {
            if (_repository.GetById(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            contact.Id = id;
            _repository.Edit(contact);
        }

        public void Delete(int id)
        {
            if (_repository.GetById(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            _repository.Delete(id);
        }
    }
}
