﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookAngularJS.DTOs;
using AddressBookAngularJS.DTOs.Interfaces;

namespace AddressBookAngularJS.Data
{
    public class InMemoryContactRepository : IContactRepository
    {
        private static List<Contact> _contacts = new List<Contact>();

        static InMemoryContactRepository()
        {
            _contacts.AddRange(new[]
            {
                new Contact() {Email = "joe@gmail.com", Id = 1, LastName = "Schmoe", FirstName = "Joe", Phone = "555-867-5309"},
                new Contact() {Email = "jane@gmail.com", Id = 2, LastName = "Doe", FirstName = "Jane", Phone = "111-222-3333"},
            });
        }
        public List<Contact> GetAll()
        {
            return _contacts;
        }

        public void Add(Contact contact)
        {
            if (_contacts.Any())
                contact.Id = _contacts.Max(c => c.Id) + 1;
            else
                contact.Id = 1;

            _contacts.Add(contact);
        }

        public void Delete(int id)
        {
            _contacts.RemoveAll(c => c.Id == id);
        }

        public void Edit(Contact contact)
        {
            Delete(contact.Id);
            _contacts.Add(contact);
        }


        public Contact GetById(int id)
        {
            return _contacts.First(c => c.Id == id);
        }
    }
}
