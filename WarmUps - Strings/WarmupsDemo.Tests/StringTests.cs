﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmUps;

namespace WarmupsDemo.Tests
{
    [TestFixture]
    public class StringTests
    {
        [Test]
        public void SayHiTest()
        {
            StringWarmups warmupObject1 = new StringWarmups();
            string test = "Alice";
            string expected = "Hello Alice!";
            string actual = warmupObject1.SayHi(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void AbbaTest()
        {
            StringWarmups warmupObject2 = new StringWarmups();
            string a = "Yo";
            string b = "Alice";
            string expected = "YoAliceAliceYo";
            string actual = warmupObject2.Abba(a, b);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void MakeTagsTest()
        {
            // Arrange
            StringWarmups warmupObject = new StringWarmups();
            string tag = "i";
            string content = "Yay";
            string expected = "<i>Yay</i>";

            // Act
            string actual = warmupObject.MakeTags(tag, content);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void InsertWordTest()
        {
            StringWarmups warmupObject2 = new StringWarmups();
            string container = "<<>>";
            string word = "WooHoo";
            string expected = "<<WooHoo>>";
            string actual = warmupObject2.InsertWord(container, word);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void MultipleEndingsTest()
        {
            StringWarmups warmupObject3 = new StringWarmups();
            string test = "Hello";
            string expected = "lololo";
            string actual = warmupObject3.MultipleEndings(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void FirstHalfTest()
        {
            // Arrange
            StringWarmups warmupObject1 = new StringWarmups();
            string message = "WooHoo";
            string expected = "Woo";

            // Act
            string actual = warmupObject1.FirstHalf(message);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void TrimOneTest()
        {
            // Arrange
            StringWarmups warmupObject2 = new StringWarmups();
            string str = "Hello";
            string expected = "ell";

            // Act
            string actual = warmupObject2.TrimOne(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void LongInMiddle()
        {
            // Arrange
            StringWarmups warmupObject3 = new StringWarmups();
            string str1 = "Hello";
            string str2 = "hi";
            string expected = "hiHellohi";

            //Act
            string actual = warmupObject3.LongInMiddle(str1, str2);

            //Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void RotateLeft2()
        {
            // Arrange
            StringWarmups warmupObject4 = new StringWarmups();
            string str = "java";
            string expected = "vaja";

            // Act
            string actual = warmupObject4.Rotateleft2(str);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void RotateRight2()
        {
            // Arrange
            StringWarmups warmupObject5 = new StringWarmups();
            string str = "Hello";
            string expected = "loHel";

            // Act
            string actual = warmupObject5.Rotateright2(str);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
