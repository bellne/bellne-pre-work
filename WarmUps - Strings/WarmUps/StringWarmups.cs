﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarmUps
{
    public class StringWarmups
    {
        // 1
        public string SayHi(string name)
        {
            string result = "Hello " + name + "!";
            return result;
        }
        // 2
        public string Abba(string a, string b)
        {
            string result = a + b + b + a;
            return result;
        }
        // 3
        public string MakeTags(string tag, string content)
        {            
            return "<" + tag + ">" + content + "</" + tag + ">";
        }
        // 4 
        public string InsertWord(string container, string word)
        {
            string firstHalf = container.Substring(0, 2);
            string secondHalf = container.Substring(2, 2);
            string result = firstHalf + word + secondHalf;
            return result;
        }
        // 5
        public string MultipleEndings(string str)
        {
            string lastTwo = str.Substring(str.Length - 2, 2);
            string result = lastTwo + lastTwo + lastTwo;
            return result;
        }
        // 6
        public string FirstHalf(string message)
        {
            
            int length = message.Length;
            int halfLength = length / 2;
            string answer = message.Substring(0, halfLength);
            return answer;
        }
        // 7
        public string TrimOne(string str)
        {
            int length = str.Length;
            string actual = str.Substring(1, length - 2);
            return actual;
        }
        // 8
        public string LongInMiddle(string a, string b)
        {
            int length1 = a.Length;
            int length2 = b.Length;

            if (length1 > length2)
            {
                string actual = b + a + b;
                return actual;
            }
            else
            {
                string actual = a + b + a;
                return actual;
            }
        }
        // 9
        public string Rotateleft2(string str)
        {
            string firstTwo = str.Substring(0, 2);
            string actual = str.Substring(2) + firstTwo;
            return actual;
        }
        // 10
        public string Rotateright2(string str)
        {
            int length = str.Length;
            string firstStr = str.Substring(length - 2, 2);
            string secondStr = str.Substring(0, length - 2);
            string actual = firstStr + secondStr;
            return actual;
        }
    }
}
