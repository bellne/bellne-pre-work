﻿using ModelBindingExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelBindingExample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult BindingClassTypes()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult BindingClassTypes(Person p)
        {
            return View("Result", p);
        }
    }
}