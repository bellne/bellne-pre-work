CREATE TABLE [dbo].[Actors](
	[ActorID][int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[FirstName][varchar](30) NOT NULL,
	[LastName][varchar](30) NOT NULL
)

CREATE TABLE [dbo].[MovieActors](
	[MovieID][int] NOT NULL,
	[ActorID][int] NOT NULL,
	Constraint PK_MovieActors PRIMARY KEY(MovieID, ActorID)
)

ALTER TABLE [dbo].[MovieActors]
	ADD CONSTRAINT FK_MovieActors_Movies FOREIGN KEY (MovieID)
	REFERENCES [dbo].[Movies] (MovieID)

ALTER TABLE [dbo].[MovieActors]
	ADD CONSTRAINT FK_MovieActors_Actors FOREIGN KEY (ActorID)
	REFERENCES [dbo].[Actors] (ActorID)