﻿using Dapper;
using FunWithDapper.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithDapper
{
    class NorthwindRepository
    {
        public List<Employee> GetAll()
        {
            List<Employee> employees = new List<Employee>();

            using(SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                //using (SqlCommand cmd = new SqlCommand())
                //{
                //    cmd.CommandText = "SELECT e1.EmployeeID, e1.LastName, e1.FirstName, e1.Title, e1.BirthDate, " +
                //                        "e1.ReportsTo, e2.LastName as ManagerName " +
                //                        "FROM Employees e1 " +
                //                            "LEFT JOIN Employees e2 " +
                //                                "ON e1.ReportsTo = e2.EmployeeID ";

                //    cmd.Connection = cn;
                //    cn.Open();

                //    using(SqlDataReader dr = cmd.ExecuteReader())
                //    {
                //        while (dr.Read())
                //        {
                //            employees.Add(PopulateFromDataReader(dr));
                //        }
                //    }
                //}

                employees = cn.Query<Employee>("SELECT e1.EmployeeID, e1.LastName, e1.FirstName, e1.Title, e1.BirthDate, " +
                                        "e1.ReportsTo, e2.LastName as ManagerName " +
                                        "FROM Employees e1 " +
                                            "LEFT JOIN Employees e2 " +
                                                "ON e1.ReportsTo = e2.EmployeeID ")
                    .ToList();
            }

            return employees;
        }

        private Employee PopulateFromDataReader(SqlDataReader dr)
        {
            Employee employee = new Employee();

            employee.EmployeeID = (int)dr["EmployeeID"];
            employee.LastName = dr["LastName"].ToString();
            employee.FirstName = dr["FirstName"].ToString();
            employee.Title = dr["Title"].ToString();

            if(dr["BirthDate"] != DBNull.Value)
            {
                employee.BirthDate = (DateTime)dr["BirthDate"];
            }

            if(dr["ReportsTo"] != DBNull.Value)
            {
                employee.ReportsTo = (int)dr["ReportsTo"];
                employee.ManagerName = dr["ManagerName"].ToString();
            }

            return employee;
        }

        public Employee GetEmployeeByID(int employeeId)
        {
            using (SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("EmployeeId", employeeId);

                Employee employee = cn.Query<Employee>("SELECT * FROM Employees " +
                                                        "WHERE EmployeeID = @EmployeeId",
                                                        //new { EmployeeId = employeeId })
                                                        parameters)
                                                        .FirstOrDefault();
                return employee;
            }
        }

        public int InsertRegion(string description)
        {
            using (SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("RegionDescription", description);
                parameters.Add("RegionId", DbType.Int32, 
                    direction: System.Data.ParameterDirection.Output);

                cn.Execute("RegionInsert", parameters, 
                    commandType: CommandType.StoredProcedure);

                int regionId = parameters.Get<int>("RegionId");

                return regionId;
            }
        }
    }
}
