﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Part_I_Smartphone_Lab
{
    class Smartphone
    {
        private string _firstName;
        private string _lastName;
        private int _phoneNumber;
        private string _email;


        public string FirstName
        {
            get {return _firstName; }
            set {_firstName = value; }
        }
        public string LastName { get; set;  }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}
