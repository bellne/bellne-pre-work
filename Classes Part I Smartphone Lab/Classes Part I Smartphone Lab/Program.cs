﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Part_I_Smartphone_Lab
{
    class Program
    {
        public List<Contacts> Phonebooks { get; set; } = new List<Contacts>();

        static void Main(string[] args)
        {
            Actions usePhone = new Actions();
            Program test = new Program();

            Contacts Contact1 = new Contacts("Nate", "Bell", "bell.nathan9@outlook.com", "7153793366");
           
            Contacts Contact2 = new Contacts("Heather", "Welter", "heattrose@gmail.com", "7155599999");
         
            test.Phonebooks.Add(Contact1);
            test.Phonebooks.Add(Contact2);
            usePhone.UserInterface(test.Phonebooks);
        }                
    }
}
