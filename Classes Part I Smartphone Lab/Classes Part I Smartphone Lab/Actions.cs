﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Part_I_Smartphone_Lab
{
    class Actions
    {
        bool callInProgress = false;
        Program test2 = new Program();
        //string _firstName;
        //string _lastName;
        //string _email;
        //int phoneNumber;

        public void UserInterface(List<Contacts> Phonebooks)
        {           
            Console.WriteLine("Choose from the options below: ");
            Console.WriteLine("1. Make Call\n2. End Call\n3. Add New Contact\n4. Show All Contacts");
            string response = Console.ReadLine();
            int responseInt = Int32.Parse(response); 
            
            if (responseInt == 1)
            {
                MakeCall();
            }
            else if (responseInt == 2)
            {
                EndCall();
            }
            else if (responseInt == 3)
            {
                //AddContact();
            }
            else if(responseInt == 4)
            {
                ShowAllContacts(Phonebooks);
            }
            else
            {
                Console.WriteLine("Invalid entry, please try again");
                Console.ReadLine();
                UserInterface(Phonebooks);
            }
        }

        public void MakeCall()
        {
            if (callInProgress == true)
            {
                Console.Clear();
                Console.WriteLine("\nThere is already a call in progress.  Maybe try ending your call instead...\n");
                //UserInterface(Phonebooks);
            }
            else
            {
                Console.Clear();
                callInProgress = true;
                Console.WriteLine("\nYour call has been placed...What next?\n");
                //UserInterface(Phonebooks);
            }
        }

        public void EndCall()
        {
            if (callInProgress == false) 
            {
                Console.Clear();
                Console.WriteLine("\nThere isn't a call in progress to end.  Please make a new selection.\n");
               // UserInterface(Phonebooks);
            }
            else
            {
                Console.Clear();
                callInProgress = false;
                Console.WriteLine("\nYour call has been ended...What next?\n");
                //UserInterface();
            }
        }

        public void ShowAllContacts(List<Contacts> Phonebooks)
        {
            Console.Clear();
            Console.WriteLine("Current Contact List: ");

            foreach (var contact in Phonebooks)
            {
                Console.WriteLine("{0} {1}", contact.LastName, contact.FirstName);
            }
            Console.ReadLine();
           // UserInterface();
        }
        public void AddContact()
        {

        }
    }
}
