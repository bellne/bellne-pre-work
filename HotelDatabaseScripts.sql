USE HotelReservationSystem
GO

--CREATE TABLE Reservation
--(
--	ReservationID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	CustomerID int NOT NULL, 
--	CheckInDate DateTime NOT NULL,
--	CheckOutDate DateTime NOT NULL,
--	TotalRoomPrice smallmoney NOT NULL,
--	Tax smallmoney NULL
--);

--CREATE TABLE Customer
--(
--	CustomerID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	FirstName varchar(20) NOT NULL,
--	LastName varchar (20) NOT NULL,
--	StreetAddress varchar (40) NOT NULL,
--	City varchar(20) NOT NULL,
--	StateHome varchar(2) NOT NULL,
--	Phone varchar(15) NOT NULL,
--	Zip int NOT NULL,
--	Email varchar(30) NULL
--);

--CREATE TABLE RoomReservation
--(
--	RoomReservationID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	RoomID int NOT NULL,
--	ReservationID int NOT NULL,
--	RoomRateID int NOT NULL,
--	RoomReservationAmenityID int NOT NULL,
--	FromDate DateTime NOT NULL,
--	ToDate DateTime NOT NULL,
--	Price smallmoney NOT NULL
--);

--CREATE TABLE RoomRate
--(
--	RoomRateID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	Price smallmoney NOT NULL,
--	FromDate DateTime NOT NULL,
--	ToDate DateTime NOT NULL
--);

--CREATE TABLE Room
--(
--	RoomID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	RoomTypeID int NOT NULL,
--	RoomNumber int NOT NULL
--);

--CREATE TABLE RoomType
--(
--	RoomTypeID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	RoomTypeName varchar(20) NOT NULL,
--	MaxOccupancy int NOT NULL
--);

--CREATE TABLE Payment
--(
--	PaymentID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	PaymentTypeID int NOT NULL,
--	CreditCardToken 
--);GO

--CREATE TABLE PaymentType
--(
--	PaymentTypeID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	VISA int NULL,
--	MASTERCARD int NULL,
--	AmericanExpress int NULL
--);GO

--CREATE TABLE Hotel
--(
--	HotelID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
--	Name varchar(30) NOT NULL,
--	City varchar(30) NOT NULL,
--	State varchar(2) NOT NULL
--);

--ALTER TABLE Room
--	ALTER COLUMN RoomNumber varchar(6) NOT NULL

--ALTER TABLE Room
--	ALTER ADD <columnName> type

--alter table room
--   add HotelID int

--ALTER TABLE Room
--	ADD FOREIGN KEY (HotelID) references Hotel(HotelID)

--INSERT INTO Hotel (Name, City, [State])
--	VALUES ('Radisson', 'Minneapolis', 'MN'),
--		('Marriott', 'Akron', 'OH'),
--		('Hilton', 'Louisville', 'KY')

--DELETE FROM Hotel WHERE HotelID = 4

--INSERT INTO Room (RoomTypeID, RoomNumber, HotelID)
--VALUES(1,'101', 1),
--	(2, '102', 1),
--	(3, '103', 1),
--	(4, '1001', 2),
--	(5, '1002', 2),
--	(6, '1003', 2),
--	(7, '10', 3),
--	(8, '11', 3),
--	(9, '12', 3)

--INSERT INTO RoomType (RoomTypeName, MaxOccupancy)
--VALUES('King', 5),
--	('Queen', 4),
--	('Suite', 6)

--UPDATE Room
--	SET RoomTypeID=3
--	WHERE RoomID = 9

--ALTER TABLE Room
--	ADD FOREIGN KEY (RoomTypeID) references RoomType(RoomTypeID)

--alter table RoomRate
--	add RoomTypeID int null

--ALTER TABLE RoomRate
--	ADD FOREIGN KEY (RoomTypeID) references RoomType(RoomTypeID)

--alter table roomrate
--	add HotelID int not null

--ALTER TABLE RoomRate
--	ADD FOREIGN KEY (HotelID) references Hotel(HotelID)

--ALTER TABLE RoomRate 
--	ADD RoomTypeID int

--ALTER TABLE RoomRate
--	ADD HotelID int

--UpdATE RoomRate	
--	SET fromdate = '1-1-2016'

--ALTER TABLE RoomRate
--	ALTER COLUMN ToDate Date NOT NULL
	

--INSERT INTO RoomRate (Price, FromDate, ToDate, RoomTypeID, HotelID)
--	VALUES 
--	('90.00', '2016-1-1', '2016-12-31', 1, 1),
--	('70.00', '2016-1-1', '2016-12-31', 2, 1),
--	('110.00', '2016-1-1', '2016-12-31', 3, 1),
--	('80.00', '2016-1-1', '2016-12-31', 1, 2),
--	('60.00', '2016-1-1', '2016-12-31', 2, 2),
--	('100.00', '2016-1-1', '2016-12-31', 3, 2),
--	('85.00', '2016-1-1', '2016-12-31', 1, 3),
--	('75.00', '2016-1-1', '2016-12-31', 2, 3),
--	('95.00', '2016-1-1', '2016-12-31', 3, 3)

--update roomrate
--	set ToDate = '12-31-2016'
--	where roomrateid = 1

--insert into roomrate (Price, FromDate, ToDate, RoomTypeID, HotelID)
--	values ('115.00', '1-1-2017', '12-31-2017', 1,1),
--	('75.00', '1-1-2017', '12-31-2017', 2, 1),
--	('115.00', '1-1-2017', '12-31-2017', 3, 1),
--	('85.00', '1-1-2017', '12-31-2017', 1, 2),
--	('65.00', '1-1-2017', '12-31-2017', 2, 2),
--	('105.00', '1-1-2017', '12-31-2017', 3, 2),
--	('90.00', '1-1-2017', '12-31-2017', 1, 3),
--	('80.00', '1-1-2017', '12-31-2017', 2, 3),
--	('100.00', '1-1-2017', '12-31-2017', 3, 3)

--update roomrate
--	set price = '95.00' where roomrateid = 10
--update roomrate
--	set price = '75.00' where roomrateid = 11
--update roomrate
--	set price = '115.00' where roomrateid = 12
--	update roomrate
--	set price ='85.00' where roomrateid = 13
--	update roomrate
--	set price ='65.00' where roomrateid = 14
--	update roomrate
--	set price ='105.00' where roomrateid = 15
--	update roomrate
--	set price ='90.00' where roomrateid = 16
--	update roomrate
--	set price = '80.00' where roomrateid = 17
--	update roomrate
--	set price ='100.00' where roomrateid = 18

--update roomrate
--set price = price -'5.00'
--where roomrate.HotelID = 1 
--and fromdate = '2017-01-01'

--ALTER TABLE RoomReservation
--ADD FOREIGN KEY (RoomRateID) REFERENCES RoomRate(RoomRateID)

--ALTER TABLE Roomreservation
--ADD FOREIGN KEY (RoomID) REFERENCES Room(RoomID)

--ALTER TABLE RoomReservation
--ADD FOREIGN KEY (ReservationID) REFERENCES Reservation(ReservationID)

--ALTER TABLE Reservation
--ADD FOREIGN KEY (CustomerID) REFERENCEs Customer(CustomerID)


--insert customer (FirstName, Lastname, StreetAddress, city, StateHome, phone, zip, email)
--	values('Tom', 'Cruise', '534 Hollywood Blvd', 'Hollywood', 'CA', '9096541122', 90210, 'tcruise@gmail.com')

--insert reservation(customerid, checkindate, checkoutdate, TotalRoomPrice, tax)
--	values(1, '3-30-2016', '3-31-2016', '110.00', '5.50')

--insert roomreservation(roomid, reservationid, roomrateid, fromdate, todate, price)
--	values(1, 1, 1, '3-30-2016', '3-31-2016', '110.00')

--SELECT*
--from Reservation

--alter table roomreservation
--	alter column RoomReservationAmenityID int NULL

--select name, hotel.city, roomnumber, price, fromdate, todate, firstname, lastname
--from roomreservation
--	INNER JOIN Reservation
--	on RoomReservation.ReservationID = Reservation.ReservationID
--	INNER JOIN Customer
--	on customer.CustomerID = reservation.CustomerID
--	INNER JOIN room
--	on room.roomid = roomreservation.RoomID
--	INNER JOIN hotel
--	on hotel.hotelid = room.HotelID


--select *
--from roomrate

--DECLARE @SomeDate datetime
--SET @SomeDate = '2016-03-30'
--SELECT City, RoomTypeName, Price, FromDate, ToDate
--from RoomRate
--	INNER JOIN RoomType
--	ON Roomrate.RoomTypeID = roomtype.RoomTypeID
--	INNER JOIN Hotel
--	on hotel.HotelID = RoomRate.hotelid
--where @SomeDate between FromDate and ToDate
--and (hotel.city = 'Minneapolis')

--DECLARE @somedate date
--SET @somedate = '3-30-17'
--SELECT City, RoomTypeName, Price, @somedate as 'ReservationDate', FromDate, ToDate
--from RoomRate
--	INNER JOIN RoomType
--	ON Roomrate.RoomTypeID = roomtype.RoomTypeID
--	INNER JOIN Hotel
--	on hotel.HotelID = RoomRate.hotelid
--where '3-30-17' between FromDate and ToDate
--and (hotel.city = 'Minneapolis')

--alter table reservation
--alter column checkoutdate date

--update room
--set roomtypeid = 3
--where roomid = 9

select *
from roomrate

select *
from roomtype

select *
from room