﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieCatalog.Models
{
    public class MovieRepository
    {
        private static List<Movie> _movies;

        static MovieRepository()
        {
            _movies = new List<Movie>
            {
                new Movie {MovieId=1, Title="Ghostbusters", Rating="PG-13", Stars=4 },
                new Movie {MovieId=2, Title="Star Wars", Rating="PG", Stars=5 },
                new Movie {MovieId=3, Title="Aristocats", Rating="G", Stars=2 }
            };
        }

        public static List<Movie> GetAll()
        {
            return _movies;
        }

        public static Movie Get(int id)
        {
            return _movies.FirstOrDefault(m => m.MovieId == id);
        }

        public static void Insert(Movie movie)
        {
            movie.MovieId = _movies.Max(m => m.MovieId) + 1;
            _movies.Add(movie);
        }

        public static void Delete(int id)
        {
            _movies.RemoveAll(m => m.MovieId == id);
        }

        public static void Edit(Movie movie)
        {
            _movies.RemoveAll(m => m.MovieId == movie.MovieId);
            _movies.Add(movie);
        }
    }
}