﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieCatalog.Models;

namespace MovieCatalog.Controllers
{
    public class MovieController : Controller
    {
        // GET is the default
        [HttpGet] // don't need this because Get is the default but this should help me remember
        public ActionResult Add()
        {
            var model = new AddMovieVM();
            model.Movie = new Movie(); //Need to make sure the object is instantiated or else it's value will be null and the program will blow up

            return View(model);
        }

        [HttpPost]
        public ActionResult PostMovie(AddMovieVM newMovie)
        {
            MovieRepository.Insert(newMovie.Movie);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MovieRepository.Delete(id);

            return RedirectToAction("Index", "Home");
        }
        
        
        public ActionResult Edit(int id)
        {
            var model = MovieRepository.Get(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Movie movie)
        {
            MovieRepository.Edit(movie);

            return RedirectToAction("Index", "Home");
        }

        //[HttpPost]
        //public ActionResult PostMovie(string Title, string Rating, int Stars)
        //{
        //    Movie newMovie = new Movie();
        //    newMovie.Title = Title;
        //    newMovie.Rating = Rating;
        //    newMovie.Stars = Stars;

        //    MovieRepository.Insert(newMovie);

        //    return RedirectToAction("Index", "Home");
        //}

        //[HttpPost]
        //public ActionResult PostMovie()
        //{
        //    Movie newMovie = new Movie();
        //    newMovie.Title = Request.Form["Title"];
        //    newMovie.Rating = Request.Form["Rating"];
        //    newMovie.Stars = int.Parse(Request.Form["Stars"]);

        //    MovieRepository.Insert(newMovie);

        //    return RedirectToAction("Index", "Home");
        //}
    }
}