use Northwind
GO

DECLARE @SelectedUnitPrice money --Declaring a variable, must use @ in front of variable name
SET @SelectedUnitPrice = 60.00

DECLARE @BeginningUnitPrice money, @EndingUnitPrice money
SET @BeginningUnitPrice = 60.00
SET @EndingUnitPrice = 100.00


select*
from products
where UnitPrice BETWEEN @BeginningUnitPrice and @EndingUnitPrice

--We built a store procedure out of this******************
DEclare @CustomerID nchar(5) --matche the type in the table that the variable is being used for
set @CustomerID = 'BOTTM'

declare @MinOrderDate datetime
declare @MaxOrderDate datetime

set @MinOrderDate = '1/1/1997'
set @MaxOrderDate = '12/31/1997'

Select * from Customers Where CustomerID = @CustomerID --get all info about this customer
--*******************************

CREATE PROCEDURE GetAllCustomers AS --create a procedure this way too

SELECT * FROM Customers

--this runs the procedure that we just created
GO

exec GetAllCustomers

--this alters a procedure
alter procedure GetAllCustomers AS

select *
from Customers
order by CompanyName desc

GO

exec GetAllCustomers
--******************************