USE SWCCorp

--SELECT *
--FROM Employee as e
--	INNER JOIN PayRates AS pr ON e.EmpID = pr.EmpID
--WHERE ManagerID = 11

--UPDATE pr
--	SET YearlySalary = YearlySalary + 1000
--FROM Employee as e
--	INNER JOIN PayRates AS pr ON e.EmpID = pr.EmpID
--WHERE ManagerID = 11

--1
--SELECT *
--FROM Employee
--WHERE Employee.EmpID = 11

--UPDATE Employee
--	SET LastName = 'Green'
--WHERE Employee.EmpID = 11

--2
--SELECT *
--FROM Employee
--	INNER JOIN Location
--	ON Employee.LocationID = Location.LocationID
--	WHERE Location.City = 'Spokane'

--UPDATE Employee
--	SET Status = 'External'
--	FROM Employee
--	INNER JOIN Location
--	ON Employee.LocationID = Location.LocationID
--	WHERE Location.City = 'Spokane'

--3
--SELECT *
--FROM Location
--WHERE Location.City = 'Seattle'

--UPDATE Location
--SET Street = '111 1st Ave'
--FROM Location
--WHERE Location.City = 'Seattle'

--4
--SELECT *
--FROM [Grant]
--	INNER JOIN Employee
--	ON [Grant].EmpID = Employee.EmpID
--	INNER JOIN Location
--	ON Employee.LocationID = Location.LocationID
--WHERE Location.City = 'Boston'

--UPDATE [Grant]
--SET Amount = 20000
--FROM [Grant]
--	INNER JOIN Employee
--	ON [Grant].EmpID = Employee.EmpID
--	INNER JOIN Location
--	ON Employee.LocationID = Location.LocationID
--WHERE Location.City = 'Boston'

--SELECT *
--FROM MgmtTraining
--WHERE MgmtTraining.ClassDurationHours > 20

--DELETE
--FROM MgmtTraining
--WHERE MgmtTraining.ClassDurationHours > 20