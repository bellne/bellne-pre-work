﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main()
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;
            string userName;
            int numGuesses = 0;

            Console.WriteLine("What's your name, partner?");
            userName = Console.ReadLine();

            Random r = new Random(); // This is System's built in random number generator
            theAnswer = r.Next(1, 21); // Get a random from 1 to 20

            do
            {
                // get player input
                Console.Write("{0}, enter your guess or Q to quit: ", userName);
                playerInput = Console.ReadLine();

                if (playerInput == "Q")
                {
                    return;
                }
                // attempt to convert to number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (playerGuess < 1 || playerGuess > 20)
                    {
                        Console.WriteLine("That number isn't between 1 and 20!");
                        continue;
                    }
                    else
                    {
                        numGuesses += 1;
                    }
                    // see if they won
                    if (playerGuess == theAnswer && numGuesses == 1)
                    {
                        Console.WriteLine("Amazing! You got it in 1 guess!! Have you thought" +
                          " about a career as a psychic?");
                        isNumberGuessed = true;
                    }
                    else if (playerGuess == theAnswer)
                    {
                        Console.WriteLine("You got it!");
                        Console.WriteLine("It took you {0} guesses", numGuesses);
                        isNumberGuessed = true;
                    }
                    
                    else
                    {
                        // they didn't win, so were they too high or too low?
                        if (playerGuess > theAnswer)
                            Console.WriteLine("Too high!");
                        else
                            Console.WriteLine("Too low!");
                    }
                }
                else
                {
                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number, {0}!", userName);
                }
            } while (!isNumberGuessed);

            Console.WriteLine("Press any key to quit.");
            Console.ReadLine();
        }
    }
}
