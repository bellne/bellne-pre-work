﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmUps;

namespace WarmupsDemo.Tests
{
    [TestFixture]
    public class ArraysTests
    { 
        [Test]
        public void FirstLast6Test()
        {
            // Arrange
            ArraysWarmups warmupObject1 = new ArraysWarmups();
            int[] numbers = new int[3];
            numbers[0] = 1;
            numbers[1] = 2;
            numbers[2] = 6;

            bool expected = true;

            // Act
            bool actual = warmupObject1.FirstLast6(numbers);            

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void SameFirstLastTest()
        {
            // Arrange
            ArraysWarmups warmupObject2 = new ArraysWarmups();
            int[] arr1 = new int[4];
            arr1[0] = 1;
            arr1[1] = 2;
            arr1[2] = 3;
            arr1[3] = 1;
            bool expected = true;

            // Act
            bool actual = warmupObject2.SameFirstLast(arr1);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void MakePiTest()
        {
            // Arrange
            ArraysWarmups warmupObject3 = new ArraysWarmups();
            int n = 3;
            int[] actual = new int[n];
            int[] expected = new int[]
            {
               3, 1, 4
            };            

            // Act
            actual = warmupObject3.MakePi(n);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void commonEndTest()
        {
            // Arrange
            ArraysWarmups warmupobject4 = new ArraysWarmups();
            int[] arra = new int[] { 1, 2, 3 };
            int[] arrb = new int[] { 7, 3 };
            bool expected = true;

            // Act
            bool actual = warmupobject4.commonEnd(arra, arrb);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void SumTest()
        {
            // Arrange
            ArraysWarmups warmupobject5 = new ArraysWarmups();
            int[] arr1 = new int[] { 5, 11, 2 };
            int expected = 18;
            int actual;

            // Act
            actual = warmupobject5.Sum(arr1);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void RotateLeftTest()
        {
            ArraysWarmups warmupObject6 = new ArraysWarmups();
            int[] test = { 5, 11, 9 };
            int[] expected = { 11, 9, 5 };
            int[] actual = warmupObject6.RotateLeft(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ReverseTest()
        {
            ArraysWarmups warmupObject7 = new ArraysWarmups();
            int[] test = { 1, 2, 3 };
            int[] expected = { 3, 2, 1 };
            int[] actual = warmupObject7.Reverse(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void HigherWinsTest()
        {
            ArraysWarmups warmupObject8 = new ArraysWarmups();
            int[] test = { 11, 5, 9 };
            int[] expected = { 11, 11, 11 };
            int[] actual = warmupObject8.HigherWins(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetMiddleTest()
        {
            ArraysWarmups warmupObject9 = new ArraysWarmups();
            int[] a = { 7, 7, 7 };
            int[] b = { 3, 8, 0 };
            int[] expected = { 7, 8 };
            int[] actual = warmupObject9.GetMiddle(a, b);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void HasEvenTest()
        {
            ArraysWarmups warmupObject10 = new ArraysWarmups();
            int[] test = { 7, 5 };
            bool expected = false;
            bool actual = warmupObject10.HasEven(test);
            Assert.AreEqual(expected, actual);
        }
    }
}
