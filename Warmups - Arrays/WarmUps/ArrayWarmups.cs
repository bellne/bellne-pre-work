﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarmUps
{
    // 1
    public class ArraysWarmups
    {
        public bool FirstLast6(int[] numbers)
        {
            int lengthOfArray = numbers.Length;
            if (numbers[0] == 6 || numbers[lengthOfArray - 1] == 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // 2
        public bool SameFirstLast(int[] numbers)
        {
            bool answer = true;

            int arrayLength = numbers.Length;
            if (arrayLength > 1)
            {
                if (numbers[0] == numbers[(arrayLength - 1)])
                {
                    answer = true;
                }
                else
                {
                    answer = false;
                }
            }
            else
            {
                answer = false;
            }

            return answer;
        }
        // 3
        public int[] MakePi(int n)
        {
            double piValue = Math.PI;
            string piValueString = piValue.ToString();
            // remove decimal
            piValueString = piValueString.Replace(".", "");
            string piValueStringReducedLength = piValueString.Substring(0, n);
            int convertToInt = Int32.Parse(piValueStringReducedLength);

            int[] arr1 = convertToInt.ToString().ToCharArray().Select(x => (int)Char.GetNumericValue(x)).ToArray();
            
            return arr1;
        }
        // 4
        public bool commonEnd(int[] a, int[] b)
        {
            bool result = false;
            int aFirstElement = a[0];
            int bFirstElement = b[0];
            int aLastElement;
            int bLastElement;
            int aLength = a.Length;
            int bLength = b.Length;
            aLastElement = a[aLength - 1];
            bLastElement = b[bLength - 1];
            
            if(aFirstElement == bFirstElement || aLastElement == bLastElement)
            {
                result = true;
            }
            else
            {
                result = false;
                return result;
            }

            return result;        
        }
        // 5
        public int Sum(int[] numbers)
        {
            int total = 0;
            int lengthOfNumbers = numbers.Length;

            for (int i = 0; i < lengthOfNumbers; i++)
            {
                total += numbers[i];
            }

            return total;
        }
        // 6
        public int[] RotateLeft(int[] numbers)
        {
            int numbersLength = numbers.Length;
            int first = numbers[0];
            int[] result = new int[numbersLength];

            for (int i = 0; i < numbersLength - 1; i++)
            {
                result[i] = numbers[i + 1];
            }

            result[numbersLength - 1] = first;

            return result;
        }
        // 7
        public int[] Reverse(int[] numbers)
        {
            int[] temp = numbers;
            Array.Reverse(temp);

            return temp;
        }
        // 8
        public int[] HigherWins(int[] numbers)
        {
            int first = numbers[0];
            int numLength = numbers.Length;
            int last = numbers[numLength - 1];

            if (first > last)
            {
                for (int i = 0; i < numLength; i++)
                {
                    numbers[i] = first;
                }
            }
            else
                for (int i = 0; i < numLength; i++)
                {
                    numbers[i] = last;
                }

            return numbers;
        }
        // 9
        public int[] GetMiddle(int[] a, int[] b)
        {
            int middleA = a[1];
            int middleB = b[1];
            int[] result = { middleA, middleB };
            return result;
        }
        // 10
        public bool HasEven(int[] numbers)
        {
            bool result = true;
            int numLength = numbers.Length;

            for (int i = 0; i < numLength; i++)
            {
                if (numbers[i] % 2 == 0)
                {
                    result = true;
                }
                else
                {

                    result = false;
                }
            }

            return result;
        }
    }
}
