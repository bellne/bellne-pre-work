﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Warmups_Loops;

namespace WarmupsLoops.Test
{
    [TestFixture]
    public class TTTTest
    {
        [Test]
        public void StringTimes()
        {
            Warmups warmupObject1 = new Warmups();
            string str = "Hi";
            int n = 3;
            string expected = "HiHiHi";
            string actual = warmupObject1.StringTimes(str, n);
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimesTest(string input, int repeat, string expected)
        {
            Warmups warmupObject2 = new Warmups();
            string actual = warmupObject2.FrontTimes(input, repeat);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void CountXX()
        {
            Warmups warmupObject3 = new Warmups();
            string str = "abcxx";
            int expected = 1;
            int actual = warmupObject3.CountXX(str);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void DoubleXTest()
        {
            Warmups warmupObject4 = new Warmups();
            string str = "axxbb";
            bool expected = true;
            bool actual = warmupObject4.DoubleX(str);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void EveryOtherTest()
        {
            Warmups warmupObject5 = new Warmups();
            string str = "Heeololeo";
            string expected = "Hello";
            string actual = warmupObject5.EveryOther(str);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void StringSplosionTest()
        {
            Warmups warmupObject6 = new Warmups();
            string splosion = "Code";
            string actual = warmupObject6.StringSplosion(splosion);
            string expected = "CCoCodCode";
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void CountLast2Test()
        {
            Warmups warmupObject7 = new Warmups();
            string test = "xaxxaxaxx";
            int actual;
            actual = warmupObject7.CountLast2(test);
            int expected = 1;
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Count9Test()
        {
            Warmups warmupObject8 = new Warmups();
            int[] test = { 1, 9, 9, 3, 9 };
            int actual = 0;
            int expected = 3;
            actual = warmupObject8.Count9(test);
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void ArrayFront9()
        {
            Warmups warmupObject9 = new Warmups();
            int[] test = { 1, 2, 3, 9, 5 };
            bool expected = true;
            bool actual = warmupObject9.ArrayFront9(test);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Array123Test()
        {
            Warmups warmupObject10 = new Warmups();
            int[] test = { 1, 1, 2, 1, 2, 3 };
            bool expected = true;
            bool actual = warmupObject10.Array123(test);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new[] { 6, 6, 2 }, 1)]
        [TestCase(new[] { 6, 6, 2, 6}, 1)]
        [TestCase(new[] { 6, 7, 2, 6}, 1)]
        public void Array667(int[] numbers, int expected)
        {
            Warmups warmupObject15 = new Warmups();
            int actual = warmupObject15.Array667(numbers);
            Assert.AreEqual(expected, actual);
        }        
    }
}
