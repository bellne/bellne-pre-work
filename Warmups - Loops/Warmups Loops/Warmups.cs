﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Warmups_Loops
{
    public class Warmups
    {
        // 1
        public string StringTimes(string str, int n)
        {
            string result = "";
            for (int i = 0; i < n; i++)
            {
                result = result + str;
            }
            return result;
        }
        // 2
        public string FrontTimes(string str, int n)
        {
            string result = "";
            int strLength = str.Length;

            //string first3Chars = (str.Length < 3 ? str : str.Substring(0, 3));

            if (strLength >= 3)
            {
                string strTrimmedToThree = str.Remove(3, strLength - 3);
                char[] strTrimmedToThreeArray = new char[3];
                strTrimmedToThreeArray = strTrimmedToThree.ToCharArray();
                string temp = new string(strTrimmedToThreeArray);

                for (int i = 0; i < n; i++)
                {
                    result += temp;
                }
                return result;
            }
            else if (strLength < 3)
            {
                for (int i = 0; i < n; i++)
                {
                    result += str;
                }
                return result;
            }
            return "";
        }
        // 3
        public int CountXX(string str)
        {
            int strCount = str.Count(c => c == 'X' || c == 'x');
            int strModPart = strCount % 2;
            int result = ((strCount - strModPart) / 2) + strModPart;
            return result;
        }
        // 4
        public bool DoubleX(string str)
        {            
            if (str.Contains("xx"))
            {
                int strLength = str.Length;
                char[] strCharArray = new char[strLength];
                strCharArray = str.ToCharArray();

                for (int i = 0; i < strLength; i++)
                {
                    if(strCharArray[i] == 'x')
                    {
                        if (strCharArray[i + 1] == 'x')
                        {
                            return true;
                        }
                        else
                            break;
                    }
                }
            }
            return false;
        }
        // 5
        public string EveryOther(string str)
        {
            int strLength = str.Length;
            int strModulo = strLength % 2;

            char[] strCharArray = new char[strLength];
            strCharArray = str.ToCharArray();
            int newArrayLength = ((strLength - strModulo) / 2) + strModulo;
            char[] resultsArray = new char[newArrayLength];

            for (int i = 0; i < newArrayLength; i++)
            {
                resultsArray[i] = strCharArray[2 * i];
            }
            string result = new string(resultsArray);
            return result;
        }
        // 6
        public string StringSplosion(string str)
        {
            int userInput = str.Length;;
            string result = "";

            for (int i = 1; i <= userInput; i++)
            {
                result = result + str.Substring(0,i);
            }

            return result;
        }
        // 7
        public int CountLast2(string str)
        {
            int length = str.Length;
            int count = 0;
            char[] strArray = str.ToArray();
            char first = strArray[length - 1];
            char second = strArray[length - 2];

            string lastTwo = (strArray[(length - 1)] + strArray[(length - 2)]).ToString();
            char[] newMethod = lastTwo.ToCharArray();
            string strShort = str.TrimEnd(newMethod);               //Substring(0, length - 2);
            char[] testString = strShort.ToArray();

            for (int i = 0; i < length - 3; i++)
            {
                if (testString[i] == first)
                {
                    if(testString[i+1] == second)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        // 8
        public int Count9(int[] numbers)
        {
            int match = 9;
            int length = numbers.Length;
            int count = 0;

            for (int i = 0; i < length; i++)
            {
                if (match == numbers[i])
                {
                    count++;
                }
            }

            return count;
        }
        // 9
        public bool ArrayFront9(int[] numbers)
        {
            int test = 9;
            int arrayLength = numbers.Length;
            bool result = false;

            if(arrayLength > 4)
            {
                arrayLength = 4;
            }

            for (int i = 0; i < arrayLength; i++)
            {
               if(numbers[i] == test)
                {
                    result = true;
                }
            }

            return result;
        }
        // 10
        public bool Array123(int[] numbers)
        {
            bool result = false;
            int test1 = 1;
            int test2 = 2;
            int test3 = 3;

            for (int i = 0; i < numbers.Length - 2; i++)
            {
                if (numbers[i] == test1)
                {
                    if (numbers[i + 1] == test2)
                    {
                        if (numbers[i + 2] == test3)
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }
        // 15
        public int Array667(int[] numbers)
        {
            int numLength = numbers.Length;
            int countOf667 = 0;

            for (int i = 0; i < numLength - 1; i++)
            {
                if(numbers[i] == 6)
                {
                    if (numbers[i+1] == 6 || numbers[i+1] == 7)
                    {
                        countOf667++;
                    }
                }
            }
            return countOf667;
        }
    }
}
