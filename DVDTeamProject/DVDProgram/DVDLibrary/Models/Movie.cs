﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models
{
    public class Movie 
    {
        public int MovieID { get; set; }

        [Required(ErrorMessage = "Please enter a Title")]
        public string Title { get; set; }

        [Required(ErrorMessage ="Please enter a Release Date")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        //[Required(ErrorMessage = "Please enter an MPAA rating")]
        public MovieMPAARating MovieMPAARating { get; set; }

        public int MPAARatingId { get; set; }
        public string DirectorName { get; set; }
        public string Studio { get; set; }

        //[Required(ErrorMessage = "Please enter a User Rating")]
        public MovieUserRating MovieUserRating { get; set; }
        public int UserRatingId { get; set; }

        public string UserNotes { get; set; }
        public string Actors { get; set; }
        public string BorrowerName { get; set; }
        public DateTime? DateBorrowed { get; set; }
        public DateTime? DateReturned { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    List<ValidationResult> errors = new List<ValidationResult>();
        //    DateTime test = new DateTime (1800-01-01);

        //    if (string.IsNullOrEmpty(Title))
        //    {
        //        errors.Add(new ValidationResult("Please enter your name", new[] { "Title" }));
        //    }

        //    if(ReleaseDate < test)
        //    {
        //        errors.Add(new ValidationResult("Please enter a correct date", new[] { "ReleaseDate" }));
        //    }

        //    return errors;
        //}
    }

    
}