﻿using System.ComponentModel.DataAnnotations;

namespace DVDLibrary.Models
{
    public class MovieUserRating
    {
        [Required(ErrorMessage = "Please enter a User Rating")]
        public int UserRatingID { get; set; }
        public int UserRating { get; set; }
        public string UserRatingDescription { get; set; }
    }
}