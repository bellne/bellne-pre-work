﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models
{
    public class MovieMPAARating
    {
        [Required(ErrorMessage = "Please enter an MPAA rating")]
        public int MPAARatingID { get; set; }
        public string MPAARating { get; set; }
        public string MPAADescription { get; set; }
    }
}