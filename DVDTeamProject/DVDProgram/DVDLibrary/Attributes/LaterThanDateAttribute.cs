﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DVDLibrary.Attributes
{
    public class LaterThanDateAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime test = new DateTime(1800 - 01 - 01);

            return base.IsValid(value) && ((DateTime)value) > test;
        }
    }
}