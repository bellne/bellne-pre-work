﻿using DVDLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.Controllers
{
    public class HomeController : Controller
    {
        IMovieRepository movieRepo;

        public HomeController(IMovieRepository repo)
        {
            movieRepo = repo;
        }

        // GET: Home 
        public ActionResult Index(string searchString)
        {
            var model = movieRepo.GetAll(searchString);

            return View(model);
        }
    }
}