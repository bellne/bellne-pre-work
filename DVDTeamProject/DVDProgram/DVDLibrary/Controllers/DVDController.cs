﻿using DVDLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.Controllers
{
    public class DVDController : Controller
    {
        //DVDController constructor implements IMovieRepository
        IMovieRepository movieRepo;

        public DVDController(IMovieRepository repo)
        {
            movieRepo = repo;
        }

        // GET: 
        public ActionResult Delete(int id)
        {
                movieRepo.Delete(id);

                return RedirectToAction("Index", "Home");      
        }

        public ActionResult Get (int id)
        {
            var model = movieRepo.Get(id);

            return View(model);
        }

        public ActionResult Add()
        {
            var model = new AddMovieVM();

            return View(model);
        }

        [HttpPost]
        public ActionResult Add(AddMovieVM model)
        {
            if (ModelState.IsValid)
            {
                movieRepo.Insert(model.Movie);
                return RedirectToAction("Index", "Home");
            }
            return View(model);

        }

        public ActionResult Edit(int id)
        {
            var model = new AddMovieVM();
            model.Movie = movieRepo.Get(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AddMovieVM model)
        {
            if (ModelState.IsValid)
            {
                movieRepo.Edit(model.Movie);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
    }
}