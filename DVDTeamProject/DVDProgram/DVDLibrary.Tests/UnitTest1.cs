﻿using System;

using DVDLibrary.Models;
using NUnit.Framework;
using System.Collections.Generic;

namespace DVDLibrary.Tests
{
    [TestFixture]
    public class UnitTest1
    {
        IMovieRepository _movieRepo = new MockMovieRepo();


        //set up a repo to run with tests
        [SetUp]
        public void SetUpForTests()
        {
            _movieRepo.Insert(new Movie { MovieID = 1, Title = "Jaws" });
            _movieRepo.Insert(new Movie { MovieID = 2, Title = "Flubber" });
        }

        //Testing Model Layer 
        [Test]
        public void TestGetAll()
        {
            //Arrange
            int expected = 2;

            //Act
            int actual = _movieRepo.GetAll("").Count;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestDelete()
        {
            //Arrange
            int expected = 1;

            //Act
            _movieRepo.Delete(1);
            int actual = _movieRepo.GetAll("").Count;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestGet()
        {
            //Arrange
            int expected = 1;

            //Act
            Movie movie = _movieRepo.Get(1);
            int actual = movie.MovieID;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
