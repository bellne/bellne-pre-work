﻿using System;
using NUnit.Framework;
using DVDLibrary.Models;
using DVDLibrary.Controllers;
using System.Web.Mvc;
using System.Collections.Generic;

namespace DVDLibraryTest.Test
{
    [TestFixture]
    public class UnitTest1
    {
        IMovieRepository _movieRepo = new MockMovieRepo();

        [SetUp]
        public void SetUpForTests()
        {
            _movieRepo.Insert(new Movie { MovieID = 1, Title = "Jaws" });
            _movieRepo.Insert(new Movie { MovieID = 2, Title = "Flubber" });

            
        }

        [TearDown]
        public void TearDownForTests()
        {
            List<Movie> movieList = _movieRepo.GetAll("");
            movieList.RemoveRange(0, movieList.Count);
        }


        [Test]
        public void EnsureAddResultIsNotNull()
        {
            DVDController mockController = new DVDController(_movieRepo);

            ViewResult result = mockController.Add() as ViewResult;

            Assert.IsNotNull(result);
        }

        [Test]
        public void EnsureDeleteResultIsNotNull()
        {
            DVDController mockController = new DVDController(_movieRepo);

            RedirectToRouteResult result = mockController.Delete(1) as RedirectToRouteResult;

            Assert.IsNotNull(result);
        }

        [Test]
        public void EnsureEditResultIsNotNull()
        {
            DVDController mockController = new DVDController(_movieRepo);            

            ViewResult result = mockController.Edit(1) as ViewResult;

            Assert.IsNotNull(result);
        }

        [Test]
        public void EnsureGetResultIsNotNull()
        {
            DVDController mockController = new DVDController(_movieRepo);

            ViewResult result = mockController.Get(1) as ViewResult;

            Assert.IsNotNull(result);
        }


        //want to test this but don't know how - this exception bubbles up to View level???
        //[Test]
        //public void EnsureExceptionThrownForDeleteNull()
        //{
        //    DVDController mockController = new DVDController(_movieRepo);

        //    var exception = Assert.Throws<NullReferenceException>(() => mockController.Delete(1000));

        //    Assert.IsTrue(exception.Message.Contains("reference not set to an instance of an object"));
        //}

//        [Test]
//        public void EnsureIndexIsNotNull()
//        {
//            HomeController mockController = new HomeController(_movieRepo);

//            ViewResult result = mockController.Index("") as ViewResult;

//            Assert.IsNotNull(result);
//=======
//            HttpStatusCodeResult actual = mockController.Get(1) as HttpStatusCodeResult;
            
//            Assert.IsNull(actual);
//>>>>>>> 2d701366b8108dd0df9b276b782cf135ce12a011
//        }



        ////Testing Model Layer 
        //[Test]
        //public void TestGetAll()
        //{
        //    //Arrange
        //    int expected = 2;

        //    //Act
        //    int actual = movieRepo.GetAll("").Count;

        //    //Assert
        //    Assert.AreEqual(expected, actual);
        //}

        //[Test]
        //public void TestDelete()
        //{
        //    //Arrange
        //    int expected = 1;

        //    //Act
        //    _movieRepo.Delete(1);
        //    int actual = _movieRepo.GetAll("").Count;

        //    //Assert
        //    Assert.AreEqual(expected, actual);
        //}

        //[Test]
        //public void TestGet()
        //{
        //    //Arrange
        //    int expected = 1;

        //    //Act
        //    Movie movie = _movieRepo.Get(1);
        //    int actual = movie.MovieID;

        //    //Assert
        //    Assert.AreEqual(expected, actual);
        //}
    }


}
