﻿using DVDLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibraryTest.Test
{
    public class MockMovieRepo : IMovieRepository

    {
        private List<Movie> testRepo = new List<Movie>();

        public void Delete(int id)
        {
            testRepo.RemoveAll(m => m.MovieID == id);
        }

        public void Edit(Movie movie)
        {
            Delete(movie.MovieID);
            Insert(movie);
        }

        public Movie Get(int MovieId)
        {
            return testRepo.SingleOrDefault(m => m.MovieID == MovieId);
        }

        public List<Movie> GetAll(string search)
        {
            return testRepo;
        }

        public void Insert(Movie movie)
        {
            testRepo.Add(movie);
        }
    }
}
