USE master 
GO 

IF EXISTS( SELECT * FROM sys.sysdatabases WHERE [name] = 'MovieCatalogue') 
DROP DATABASE MovieCatalogue 
GO 

CREATE DATABASE MovieCatalogue 
GO 

USE MovieCatalogue 
GO 
CREATE TABLE Movie 
( 
	MovieID INT IDENTITY(1,1) PRIMARY KEY, 
	Title VARCHAR(30) NOT NULL, 
	Runtime INT, 
	Rating VARCHAR(5)
) 
GO 

INSERT INTO Movie VALUES 
	('A-List Explorers', 96,'PG-13'), 
	('Bonker Bonzo', 75,'G'), 
	('Chumps to Champs', 75,'PG-13'), 
	('Dare or Die', 110,'R'), 
	('EeeeGhads', 88,'G')

	SELECT* FROM Movies

	delete FROM Movie WHERE Title = 'EeeeGhads'

	ALTER TABLE Movie
		DROP COLUMN Runtime

	alter table movie  --adds a column
		add [Description] varchar(100) NULL

	alter table movie
		drop column [Description]

	alter table Movie
		Add [Description] varchar(100) NOT NULL
		default 'Description coming soon'

	EXEC sp_rename 'Movie.Description', 'Teaser' --changes the name of a column

	EXEC sp_rename 'Movie', 'Movies' --changes the name of the table

	alter table Movies
		alter column Teaser VARCHAR(200) NOT NULL --changes the type of a table column

