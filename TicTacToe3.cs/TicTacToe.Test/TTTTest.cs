﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TicTacToe.cs;
using TicTacToe;

namespace TicTacToe.Test
{
    [TestFixture]
    class TTTTest
    {
        [Test]
        public void PlaceMarkTest()
        {
            // Arrange
            TicTacToe tictac = new TicTacToe();

            Player player1 = new Player()
            {
                playerName = "Nate",
                mark = "X",
                num = 2
            };
           
            string[,] GameBoard = new string[5, 5]
             {
                    { " 1 ", " | ", " 2 ", " | ", " 3 " },
                    { "---", "---", "---", "---", "---" },
                    { " 4 ", " | ", " 5 ", " | ", " 6 " },
                    { "---", "---", "---", "---", "---" },
                    { " 7 ", " | ", " 8 ", " | ", " 9 " }
             };
            string[,] expected = new string[5, 5]
             {
                    { " 1 ", " | ", " X ", " | ", " 3 " },
                    { "---", "---", "---", "---", "---" },
                    { " 4 ", " | ", " 5 ", " | ", " 6 " },
                    { "---", "---", "---", "---", "---" },
                    { " 7 ", " | ", " 8 ", " | ", " 9 " }
             };

            // Act
            string[,] actual = tictac.PlaceMarkOntoBoard(2, player1, GameBoard);

            // Assert
            Assert.AreEqual(expected, actual);
        }


    }
}
