﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.cs
{
    public class Program
    {
        static void Main(string[] args)
        {
            int x = 1;
            
            // Creat a new object of the TicTacToe class
            TicTacToe GamePlay = new TicTacToe();

            // Welcome Message
            bool whoStarts = GamePlay.Welcome();
            if (whoStarts == true)
            {
                x = 1;
            }
            else
            {
                x = 2;
            }
            
            GamePlay.Execute(x);
        }
    }
}
