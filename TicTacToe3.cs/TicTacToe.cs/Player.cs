﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.cs
{
    public class Player
    {
        public string playerName { get; set; }
        public string mark { get; set; }
        public bool torf { get; set; }
        public int num { get; set; }
    }
}
