﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.cs;

namespace TicTacToe
{
    public class TicTacToe
    {
        private string p1 = "";
        private string p2 = "";

        Player player1 = new Player()
        {
            playerName = "",
            mark = "X",
            torf = true,
            num = 2
        };
        Player player2 = new Player()
        {
            playerName = "",
            mark = "O",
            torf = false,
            num = 1
        };

        // Welcome method is used by the Program Class so it must be public
        public bool Welcome()
        {
            Console.WriteLine("Welcome to Tic Tac Toe!");

            //Get names of Player 1 and Player 2 and store in variables
            Console.WriteLine("Enter Player 1's name: ");
            player1.playerName = Console.ReadLine();
            Console.WriteLine("Now enter Player 2's name: ");
            player2.playerName = Console.ReadLine();

            //Randomly determine who goes first   
            int firstTurn = GoesFirst();
            if (firstTurn == 0)
            {
                GameKickoff(player1);
                return player1.torf;              
            }
            else
            {
                GameKickoff(player2);
                return player2.torf;            
            }            
        }

        public bool GameKickoff(Player player)
        {
            Console.WriteLine("{0} goes first!" +
                   " Press Enter to start playing", player.playerName);
            Console.ReadLine();
            Console.Clear();
            return true;
        }

        string[,] GameBoard = new string[5, 5]
         {
                { " 1 ", " | ", " 2 ", " | ", " 3 " },
                { "---", "---", "---", "---", "---" },
                { " 4 ", " | ", " 5 ", " | ", " 6 " },
                { "---", "---", "---", "---", "---" },
                { " 7 ", " | ", " 8 ", " | ", " 9 " }
         };

        // Display method is only used within the TicTacToe class so I made it private
        // Displays the gameboard to the console by iterating through the GameBoard array
        private void Display()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write("{0}", GameBoard[i, j]);
                    if (j == 4)
                    {
                        Console.Write("\n");
                    }
                }
            }
        }

        // Execute is used by the Program Class so it must be public
        // Cycles through player turns by using the values 1 and 2
        public void Execute(int x)
        {
            Display();
            do
            {
                if (x == 1)
                {
                    x = PlayerPrompt(player1);
                }
                else if (x == 2)
                {
                    x = PlayerPrompt(player2);
                }
            }
            while (x == 1 || x == 2);
        }

        // Player1Prompt method is only used within the TicTacToe class so I made it private
        private int PlayerPrompt(Player player)
        {
            int Oh = GetCoordinate(player);
            GameBoard = PlaceMarkOntoBoard(Oh, player, GameBoard);                  
            Console.Clear();
            Display();
            if (CheckForWinner(player))
            {
                CongratulateWinner(player);               
            }
            else if (CheckForTie())
            {
                Console.WriteLine("\nIt's a tie!!");
                Console.WriteLine("Press 'Y' and Enter to Play again or press 'Q' then Enter to Quit");
                string response = Console.ReadLine();
                if (response == "Y")
                {
                    Console.Clear();
                    Display();
                    int x = 1;
                    Execute(x);
                }
                else
                {
                    return 0;
                }
                Console.ReadLine();
                return 0;
            }

            // Returning player.num transfers the turn back to the other player
            return player.num;
        }

        public int GetCoordinate(Player player)
        {
            Console.WriteLine("{0} ({1}'s), enter the number for your '{2}' and then press Enter.", player.playerName, player.mark, player.mark);
            string temp = Console.ReadLine();
            //Display();
            int Oh = Int32.Parse(temp);
            return Oh;
        }

        public string[,] PlaceMarkOntoBoard(int Oh, Player player, string[,] board)
        {
            switch (Oh)
            {
                case 1:
                    if (board[0, 0] == $" {player1.mark} " || board[0, 0] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 0, 0);
                        return board;
                    }
                case 2:
                    if (board[0, 2] == $" {player1.mark} " || board[0, 2] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 0, 2);
                        return board;
                    }
                case 3:
                    if (board[0, 4] == $" {player1.mark} " || board[0, 4] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 0, 4);
                        return board;
                    }
                case 4:
                    if (board[2, 0] == $" {player1.mark} " || board[2, 0] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 2, 0);
                        return board;
                    }
                case 5:
                    if (board[2, 2] == $" {player1.mark} " || board[2, 2] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 2, 2);
                        return board;
                    }
                case 6:
                    if (board[2, 4] == $" {player1.mark} " || board[2, 4] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 2, 4);
                        return board;
                    }
                case 7:
                    if (board[4, 0] == $" {player1.mark} " || board[4, 0] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 4, 0);
                        return board;
                    }
                case 8:
                    if (board[4, 2] == $" {player1.mark} " || board[4, 2] == $" {player2.mark} ")
                    {
                        ;
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 4, 2);
                        return board;
                    }
                case 9:
                    if (board[4, 4] == $" {player1.mark} " || board[4, 4] == $" {player2.mark} ")
                    {
                        Console.WriteLine("That space was already picked, try again.");
                        PlayerPrompt(player);
                        return board;
                    }
                    else
                    {
                        board.SetValue($" {player.mark} ", 4, 4);
                        return board;
                    }
                default:
                    return board;
            }
        }

        public bool CheckForWinner(Player player)
        {
            if  ((GameBoard[0, 0] == $" {player.mark} " && GameBoard[0, 2] == $" {player.mark} " && GameBoard[0, 4] == $" {player.mark} ")
                || (GameBoard[0, 0] == $" {player.mark} " && GameBoard[2, 0] == $" {player.mark} " && GameBoard[4, 0] == $" {player.mark} ")
                || (GameBoard[0, 0] == $" {player.mark} " && GameBoard[2, 2] == $" {player.mark} " && GameBoard[4, 4] == $" {player.mark} ")
                || (GameBoard[0, 2] == $" {player.mark} " && GameBoard[2, 2] == $" {player.mark} " && GameBoard[4, 2] == $" {player.mark} ")
                || (GameBoard[0, 4] == $" {player.mark} " && GameBoard[2, 4] == $" {player.mark} " && GameBoard[4, 4] == $" {player.mark} ")
                || (GameBoard[2, 0] == $" {player.mark} " && GameBoard[2, 2] == $" {player.mark} " && GameBoard[2, 4] == $" {player.mark} ")
                || (GameBoard[4, 0] == $" {player.mark} " && GameBoard[4, 2] == $" {player.mark} " && GameBoard[4, 4] == $" {player.mark} ")
                || (GameBoard[0, 4] == $" {player.mark} " && GameBoard[2, 2] == $" {player.mark} " && GameBoard[4, 0] == $" {player.mark} "))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CongratulateWinner(Player player)
        {
            Console.WriteLine("\n{0} ({1}'s) Wins!  Congratulations!!!", player.playerName, player.mark);
            Console.WriteLine("Press 'Y' and Enter to Play again or press 'Q' then Enter to Quit");
            string response = Console.ReadLine();
            if (response.ToUpper() == "Y")
            {
                Console.Clear();
                Display();
                int x = 1;
                Execute(x);
            }
            Console.ReadLine();
        }

        public bool CheckForTie()
        {
            if
                ((GameBoard[0, 0] == $" {player1.mark} " || GameBoard[0, 0] == $" {player2.mark} ") &&
                (GameBoard[0, 2] == $" {player1.mark} " || GameBoard[0, 2] == $" {player2.mark} ") &&
                (GameBoard[0, 4] == $" {player1.mark} " || GameBoard[0, 4] == $" {player2.mark} ") &&
                (GameBoard[2, 0] == $" {player1.mark} " || GameBoard[2, 0] == $" {player2.mark} ") &&
                (GameBoard[2, 2] == $" {player1.mark} " || GameBoard[2, 2] == $" {player2.mark} ") &&
                (GameBoard[2, 4] == $" {player1.mark} " || GameBoard[2, 4] == $" {player2.mark} ") &&
                (GameBoard[4, 0] == $" {player1.mark} " || GameBoard[4, 0] == $" {player2.mark} ") &&
                (GameBoard[4, 2] == $" {player1.mark} " || GameBoard[4, 2] == $" {player2.mark} ") &&
                (GameBoard[4, 4] == $" {player1.mark} " || GameBoard[4, 4] == $" {player2.mark} "))
            {
                return true;
            } 
            else
            {
                return false;
            }
        }

        // GoesFirst is only used within the TicTacToe class so I made it private
        // Returns random number ranging from 0 - 1 to determine which player goes first
        private int GoesFirst()
        {
            Random turn1 = new Random();
            int x = turn1.Next(0, 2);
            return x;
        }
    }
}
