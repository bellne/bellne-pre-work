﻿using System;
using System.Data.SqlClient;
using System.Configuration;

namespace DataReaderExample
{
    class Program
    {
        static void Main()
        {
            DisplayEmployeeList();
            Console.ReadLine();
        }

        private static void DisplayEmployeeList()
        {
            string connectionString;
            connectionString = ConfigurationManager                
                                    .ConnectionStrings["Northwind"]
                                    .ConnectionString;

            // create connection with connection string
            // using to automatically close it
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    // Create a command
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandText = "SELECT * FROM Employees";
                    sqlCommand.Connection = sqlConnection;

                    sqlConnection.Open(); // must have open connection to query

                    // call ExecuteReader to have the command create a 
                    // data reader, this executes the SQL Statement
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        //Read() returns false when there is no more data
                        while (sqlDataReader.Read())
                        {
                            Console.WriteLine("{0} {1}, {2}",
                                sqlDataReader["EmployeeID"].ToString(),
                                sqlDataReader["LastName"].ToString(),
                                sqlDataReader["FirstName"].ToString());                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Enter to continue...");
                Console.ReadLine();
            }

        }
    }
}
