--orders by last name in alphabetical order
select FirstName, LastName
from Employee
order by LastName

--orders by last name in descending alphabetical order
select FirstName, LastName
from Employee
order by LastName desc, FirstName desc

--order by null values
select * FROM Employee
where status is null
order by LocationID, LastName, FirstName

select FirstName, LastName, City
from Employee
	INNER JOIN Location 
	ON employee.locationid = location.locationid
where status IS NOT NULL
order by LastName, FirstName
