﻿using MovieCatalogFromScratch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieCatalogFromScratch.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        public ActionResult Delete(int id)
        {
            MovieRepo.Delete(id);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Add(Movie movie)
        {
            var model = new Movie();

            return View(model);
        }

        [HttpPost]
        public ActionResult PostMovie(Movie movie)
        {
            MovieRepo.Insert(movie);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int id)
        {
            Movie model = MovieRepo.GetMovie(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit (Movie movie)
        {
            MovieRepo.Edit(movie);

            return RedirectToAction("Index", "Home");
        }
    }
}