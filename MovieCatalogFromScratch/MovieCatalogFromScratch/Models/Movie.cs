﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieCatalogFromScratch.Models
{
    public class Movie
    {
        public string Title { get; set; }
        public int stars { get; set; }
        public string Rating { get; set; }
        public int MovieId { get; set; }
    }
}