﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieCatalogFromScratch.Models
{
    public class MovieRepo
    {
        private static List<Movie> _movieRepo = new List<Movie>
        {
            new Movie {Title = "Man on Fire", Rating = "R", stars = 3, MovieId=1 },
            new Movie {Title = "Roadhouse", Rating = "R", stars = 4, MovieId=2 }
        };            

        public static List<Movie> GetAllMovies()
        {
            return _movieRepo;
        }

        public static void Delete(int id)
        {
            _movieRepo.RemoveAll(m => m.MovieId == id);
        }

        public static void Insert (Movie movie)
        {
            movie.MovieId = _movieRepo.Max(m => m.MovieId) + 1;
            _movieRepo.Add(movie);
        }

        public static void Edit (Movie movie)
        {
            _movieRepo.RemoveAll(m => m.MovieId == movie.MovieId);
            _movieRepo.Add(movie);
        }

        public static Movie GetMovie(int id)
        {
            Movie movie = _movieRepo.FirstOrDefault(m => m.MovieId == id);

            return movie;
        }
    }
}